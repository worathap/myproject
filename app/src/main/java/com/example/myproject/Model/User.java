package com.example.myproject.Model;

public class User {
    String User,Password;

    public User(String user, String password) {
        User = user;
        Password = password;
    }

    public User() {
    }

    public String getUser() {
        return User;
    }

    public void setUser(String user) {
        User = user;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
