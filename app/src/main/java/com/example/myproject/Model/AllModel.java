package com.example.myproject.Model;

import java.util.List;

public class AllModel {


    List<LightModel> lightModel;
    List<FanModel> fanModel;
    List<AirModel> airModel;

    public AllModel() {
    }

    public AllModel(List<LightModel> lightModel, List<FanModel> fanModel, List<AirModel> airModel) {
        this.lightModel = lightModel;
        this.fanModel = fanModel;
        this.airModel = airModel;
    }
}
