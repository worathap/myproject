package com.example.myproject.Model;

public class AirModel {
    String name;
    String id;
    String typename;
    boolean status;
    int watt;

    public AirModel() {
    }

    public AirModel(String name, String id,String typename, boolean status, int watt) {
        this.name = name;
        this.id = id;
        this.typename = typename;
        this.status = status;
        this.watt = watt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getWatt() {
        return watt;
    }

    public void setWatt(int watt) {
        this.watt = watt;
    }
}
