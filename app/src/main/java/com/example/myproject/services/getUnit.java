package com.example.myproject.services;

import androidx.annotation.Keep;

@Keep
public class getUnit {

    float total;
    String device_name;
    String typename;

    public getUnit() {
    }

    public getUnit(float total, String device_name,String typename) {
        this.total = total;
        this.device_name = device_name;
        this.typename = typename;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public String getDevice_name() {
        return device_name;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }
}
