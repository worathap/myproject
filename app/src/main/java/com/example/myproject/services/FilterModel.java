package com.example.myproject.services;

public class FilterModel {
    String day;
    String typename;
    float total;

    public FilterModel() {
    }

    public FilterModel(String day,String typename, float total) {
        this.day = day;
        this.typename = typename;
        this.total = total;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
