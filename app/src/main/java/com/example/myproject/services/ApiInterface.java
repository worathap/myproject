package com.example.myproject.services;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiInterface {


    @FormUrlEncoded
    @POST("/iot/public/api/create")
    Call<List<AllDevice>> create(
            @Field("uid") String uid,
            @Field("device_id") String device_id,
            @Field("device_name") String device_name,
            @Field("typename")String typename,
            @Field("device_watt") int device_watt
            );

    @FormUrlEncoded
    @POST("/iot/public/api/update/{id}")
    Call<List<AllDevice>> update(
            @Path("id")String id,
            @Field("typename") String typename
    );

    //filter
    @FormUrlEncoded
    @POST("/iot/public/api/getData")
    Call<List<AllDevice>> getData(
            @Field("typename") String typename
    );

    @FormUrlEncoded
    @POST("/iot/public/api/filterByDay")
    Call<List<FilterModel>> getDataByDay(
            @Field("uid") String uid,
            @Field("typename") String typename,
            @Field("p_first") String p_first,
            @Field("p_last") String p_last
    );

    @FormUrlEncoded
    @POST("/iot/public/api/filterByMonth")
    Call<List<FilterModel>> getDataByMonth(
            @Field("uid") String uid,
            @Field("typename") String typename,
            @Field("f_month") String f_month,
            @Field("l_month") String l_month

    );

    @FormUrlEncoded
    @POST("/iot/public/api/filterByYear")
    Call<List<FilterModel>> getDataByYear(
            @Field("uid") String uid,
            @Field("typename") String typename,
            @Field("f_year") String f_year,
            @Field("l_year") String l_year
    );

    //filterAllDevice
    @FormUrlEncoded
    @POST("/iot/public/api/filterAllByDay")
    Call<List<FilterModel>> getDataAllByDay(
            @Field("uid") String uid,
            @Field("p_first") String p_first,
            @Field("p_last") String p_last
    );

    @FormUrlEncoded
    @POST("/iot/public/api/filterAllByMonth")
    Call<List<FilterModel>> getDataAllByMonth(
            @Field("uid") String uid,
            @Field("f_month") String f_month,
            @Field("l_month") String l_month
    );

    @FormUrlEncoded
    @POST("/iot/public/api/filterAllByYear")
    Call<List<FilterModel>> getDataAllByYear(
            @Field("uid") String uid,
            @Field("f_year") String f_year,
            @Field("l_year") String l_year
    );

}
