package com.example.myproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myproject.Model.AllModel;
import com.example.myproject.Model.User;
import com.example.myproject.fragment.FragmentHome;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pixplicity.easyprefs.library.Prefs;

public class SignInActivity extends AppCompatActivity {

    EditText editTextEmail,editTextPassword;
    Button btnSignIn;
    TextView textView,forgotPass;
    FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        firebaseAuth = FirebaseAuth.getInstance();
        editTextEmail = findViewById(R.id.email);
        editTextPassword = findViewById(R.id.password);
        btnSignIn =findViewById(R.id.btn_sign_in);
        /*textView = findViewById(R.id.sign_up_Text_view);
        forgotPass = findViewById(R.id.tv_forgot);*/
        this.checkLogin();

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = editTextEmail.getText().toString();
                final String password = editTextPassword.getText().toString();

                if (TextUtils.isEmpty(email)){
                    editTextEmail.setError("กรุณาตรวจสอบรหัสอุปกรณ์");
                    editTextEmail.requestFocus();
                }
                else if (TextUtils.isEmpty(password)){
                    editTextPassword.setError("กรุณาตารวจสอบรหัสเชื่อมต่ออุปกรณ์");
                    editTextPassword.requestFocus();
                }
                else if (email.isEmpty() && password.isEmpty()){
                    Toast.makeText(SignInActivity.this, "กรุณาตรวจสอบข้อมูลอีกครั้ง", Toast.LENGTH_SHORT).show();
                }
                else if (!(email.isEmpty()&& password.isEmpty())){

                    FirebaseDatabase.getInstance().getReference().child("User/"+email).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            User user = dataSnapshot.getValue(User.class);
                            if (email.equals(user.getUser()) && password.equals(user.getPassword())){
                                Prefs.putString("Id", user.getUser());
                                Prefs.putString("Pass",user.getPassword());
                                Toast.makeText(SignInActivity.this, "เข้าสู่ระบบแล้ว", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(getApplicationContext(),MainActivity.class));
                                Log.d("TAG1", "id: "+user.getUser()+" pass: "+user.getPassword());
                            }
                            Log.d("TAG", "id: "+user.getUser()+" pass: "+user.getPassword());
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            Log.d("TAG", "onCancelled() returned: " + databaseError.getMessage());
                        }
                    });
                }
                else {
                    Toast.makeText(SignInActivity.this, "ล้มเหลว", Toast.LENGTH_SHORT).show();
                }
            }
        });


        //goSignUp TextView
        /*textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goSignUp = new Intent(SignInActivity.this,SignUpActivity.class);
                startActivity(goSignUp);
            }
        });*/

        //goSignUp TextView
        /*forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resetpassword = new Intent(SignInActivity.this,ResetPasswordActivity.class);
                startActivity(resetpassword);
            }
        });*/
    }

    public void checkLogin(){
        if (!Prefs.getString("Id", "").isEmpty()) {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
    }

}
