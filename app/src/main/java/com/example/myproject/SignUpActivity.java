package com.example.myproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.service.autofill.OnClickAction;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myproject.Model.AirModel;
import com.example.myproject.Model.AllModel;
import com.example.myproject.Model.FanModel;
import com.example.myproject.Model.LightModel;
import com.example.myproject.Model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class SignUpActivity extends AppCompatActivity {

    EditText edit_Email,edit_Password,edit_RetypePassword;
    Button BtnSignUp;
    TextView TvSignIn;

    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        edit_Email = findViewById(R.id.edit_email);
        edit_Password = findViewById(R.id.edit_password);
        edit_RetypePassword = findViewById(R.id.edit_retypepassword);
        BtnSignUp = findViewById(R.id.btn_sign_up);
        TvSignIn = findViewById(R.id.sign_in_Text_view);

        databaseReference = FirebaseDatabase.getInstance().getReference("User");
        firebaseAuth = FirebaseAuth.getInstance();

        BtnSignUp.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ShowToast")
            @Override
            public void onClick(View v) {
                final String email =edit_Email.getText().toString().trim();
                final String password = edit_Password.getText().toString().trim();
                final String retypepassword = edit_RetypePassword.getText().toString().trim();

                if (TextUtils.isEmpty(email)){
                    Toast.makeText(SignUpActivity.this,"กรุณากรอกอีเมล์",Toast.LENGTH_LONG);
                    return;
                }

                else if (TextUtils.isEmpty(password)){
                    Toast.makeText(SignUpActivity.this,"กรุณากรอกรหัสผ่าน",Toast.LENGTH_LONG);
                    return;
                }
                else if (TextUtils.isEmpty(retypepassword)){
                    Toast.makeText(SignUpActivity.this,"กรุณากรอกรหัสผ่านให้ตรงกัน",Toast.LENGTH_LONG);
                    return;
                }
                firebaseAuth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {


                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {

                                    List<LightModel> lightModels = new ArrayList<>();
                                    List<FanModel> fanModels = new ArrayList<>();
                                    List<AirModel> airModels = new ArrayList<>();

                                    for (int i =0;i<4;i++){
                                        lightModels.add(new LightModel(
                                                "light"+i,
                                                ""+i,
                                                "light",
                                                false,
                                                20
                                        ));
                                    }

                                    for (int i =0;i<2;i++){
                                        fanModels.add(new FanModel(
                                                "fan"+i,
                                                ""+i,
                                                "fan",
                                                false,
                                                20
                                        ));
                                    }

                                    for (int i =0;i<2;i++){
                                        airModels.add(new AirModel(
                                                "air"+i,
                                                ""+i,
                                                "air",
                                                false,
                                                20
                                        ));
                                    }

                                } else {

                                }

                                // ...
                            }
                        });

            }
        });

        TvSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goSigIn= new Intent(SignUpActivity.this,SignInActivity.class);
                startActivity(goSigIn);
            }
        });
    }



}
