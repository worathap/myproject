package com.example.myproject;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanQRcodeActivity extends AppCompatActivity  {

    @SuppressLint("StaticFieldLeak")
    public static TextView resultTv;
    Button scanbtn,savebtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qrcode);

        resultTv = findViewById(R.id.QRcode_txt);
        scanbtn = findViewById(R.id.btn_scan_QRcode);
        savebtn = findViewById(R.id.btn_save_QRcode);

        scanbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),ShowQRcodeActivity.class));
            }
        });

    }



}
