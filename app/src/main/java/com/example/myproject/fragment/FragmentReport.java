package com.example.myproject.fragment;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.HorizontalScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myproject.R;
import com.example.myproject.services.AllDevice;
import com.example.myproject.services.ApiClient;
import com.example.myproject.services.ApiInterface;
import com.example.myproject.services.FilterModel;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.pixplicity.easyprefs.library.Prefs;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentReport extends Fragment implements AdapterView.OnItemSelectedListener ,View.OnClickListener {

    String TAG = "FragmentReport";
    private ApiInterface apiInterface;

    BarChart barChart;
    Spinner devicetype, filtertype;
    Button btn_lightchart, btn_airchart, btn_fanchart,btn_filter;
    TextView tv_datestart,tv_dateend,tv_month,tv_monthend,tv_year,tv_yearend,txtw,tv_desc_dvname,tv_desc_datetime,tv_desc_unit,tv_unit,tv_time;

    HorizontalScrollView hsv_tv;

    //filter
    Call<List<AllDevice>> callfillter;

    List<AllDevice> list = new ArrayList<>();

    ArrayList<BarEntry> barEntries = new ArrayList<>();
    ArrayList<PieEntry> pieEntries = new ArrayList<>();

    //FilterByDay Month Year
    List<FilterModel> dayList = new ArrayList<>();
    List<FilterModel> monthList = new ArrayList<>();
    List<FilterModel> yearList = new ArrayList<>();
    Call<List<FilterModel>> callDay;
    Call<List<FilterModel>> callMonth;
    Call<List<FilterModel>> callYear;

    //FilterAllDevice
    List<FilterModel> AlldayList = new ArrayList<>();
    List<FilterModel> AllmonthList = new ArrayList<>();
    List<FilterModel> AllyearList = new ArrayList<>();
    Call<List<FilterModel>> callAllDay;
    Call<List<FilterModel>> callAllMonth;
    Call<List<FilterModel>> callAllYear;

//    List<String> name = new ArrayList<>();


    String title = "";
    String devicename ="" ;
    String uid;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_report, container, false);

        barChart = v.findViewById(R.id.barchart);
        devicetype = v.findViewById(R.id.device_type);
        filtertype = v.findViewById(R.id.filter_type);
        //button card ด้านบน3 ตัว
        btn_lightchart = v.findViewById(R.id.btn_lightchart);
        btn_airchart = v.findViewById(R.id.btn_airchart);
        btn_fanchart = v.findViewById(R.id.btn_fanchart);
        //filter button ปุ่มคำนวณ
        btn_filter = v.findViewById(R.id.btn_filter);
        // TextView Date calendar เลือกวันที่ ต่างๆ
        tv_datestart = v.findViewById(R.id.tv_datestart);
        tv_dateend = v.findViewById(R.id.tv_dateend);
        tv_month = v.findViewById(R.id.tv_month);
        tv_monthend = v.findViewById(R.id.tv_monthend);
        tv_year = v.findViewById(R.id.tv_year);
        tv_yearend = v.findViewById(R.id.tv_yearend);
//        txtw = v.findViewById(R.id.txtw); ถ้าเปิดก็จะยังใช้ได้ ปกติ เพราะไม่ได้ลบมันทิ้งไป
        //description device useDateTime and usingUnit
        tv_desc_dvname=v.findViewById(R.id.tv_desc_dvname);
        tv_desc_datetime = v.findViewById(R.id.tv_desc_datetime);
        tv_desc_unit = v.findViewById(R.id.tv_desc_unit);

        tv_unit = v.findViewById(R.id.tv_unit);//text คำว่า (หน่วย) แกน Y
        tv_time = v.findViewById(R.id.tv_time);//text คำว่า (เวลา) แกน X


        //horizontal view & ConstraintLayout
        hsv_tv = v.findViewById(R.id.hsv_tv);

//        uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        uid = Prefs.getString("Id", "null");


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        btn_lightchart.setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View v) {
                barChart.fitScreen();
                list.clear();
                barEntries.clear();
                summary=0;

                getDataFilter("light","สถิติการใช้หลอดไฟ");

//                txtw.setText("unit รวมของการใช้หลอดไฟ");
                tv_desc_dvname.setText("รายงานการใช้ไฟฟ้าของ:"+getTitle("light"));

                btn_lightchart.setBackgroundResource(R.drawable.btn_g);
                btn_airchart.setBackgroundResource(R.drawable.btn_round_white);
                btn_fanchart.setBackgroundResource(R.drawable.btn_round_white);

                btn_lightchart.setTextColor(getResources().getColor(R.color.colorWhite));
                btn_airchart.setTextColor(getResources().getColor(R.color.colorBack));
                btn_fanchart.setTextColor(getResources().getColor(R.color.colorBack));
                }
        });
        btn_airchart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.clear();
                barEntries.clear();
                summary=0;

                getDataFilter("air","สถิติการใช้แอร์");
//                txtw.setText("unit รวมของการใช้แอร์");
                tv_desc_dvname.setText("รายงานการใช้ไฟฟ้าของ:"+getTitle("air"));

                btn_lightchart.setBackgroundResource(R.drawable.btn_round_white);
                btn_airchart.setBackgroundResource(R.drawable.btn_g);
                btn_fanchart.setBackgroundResource(R.drawable.btn_round_white);

                btn_airchart.setTextColor(getResources().getColor(R.color.colorWhite));
                btn_lightchart.setTextColor(getResources().getColor(R.color.colorBack));
                btn_fanchart.setTextColor(getResources().getColor(R.color.colorBack));
            }
        });
        btn_fanchart.setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View v) {
                list.clear();
                barEntries.clear();
                summary=0;
                getDataFilter("fan","สถิติการใช้พัดลม");
//                txtw.setText("unit รวมของการใช้พัดลมดูดอากาศ");
                tv_desc_dvname.setText("รายงานการใช้ไฟฟ้าของ:"+getTitle("fan"));

                btn_lightchart.setBackgroundResource(R.drawable.btn_round_white);
                btn_airchart.setBackgroundResource(R.drawable.btn_round_white);
                btn_fanchart.setBackgroundResource(R.drawable.btn_g);

                btn_lightchart.setTextColor(getResources().getColor(R.color.colorBack));
                btn_airchart.setTextColor(getResources().getColor(R.color.colorBack));
                btn_fanchart.setTextColor(getResources().getColor(R.color.colorWhite));
                }
        });

        //device Type spinner

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.device_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        devicetype.setAdapter(adapter);
        devicetype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                txtw.setText("");
                tv_desc_dvname.setText("");
                tv_desc_datetime.setText("");
                tv_desc_unit.setText("");
                barChart.setVisibility(View.GONE);
                tv_desc_dvname.setVisibility(View.GONE);
                tv_desc_datetime.setVisibility(View.GONE);
                tv_desc_unit.setVisibility(View.GONE);

                tv_unit.setVisibility(View.GONE);
                tv_time.setVisibility(View.GONE);


                devicename = parent.getSelectedItem().toString().trim();
                Log.d(TAG, "onItemSelected: "+devicename);
                switch (devicename){
                    case "หลอดไฟ":
                        title = "light";
                        break;
                    case "แอร์":
                        title = "air";
                        break;
                    case "พัดลมระบายอากาศ":
                        title = "fan";
                        break;
                        //เพิ่มมา20201016
                    case"ทั้งหมด":
                        title="all";
                        break;
                    case "เลือกประเภทอุปกรณ์":
                        title="selectdevice";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<CharSequence> adapter_filtertype = ArrayAdapter.createFromResource(getActivity(), R.array.filter_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        filtertype.setAdapter(adapter_filtertype);
        filtertype.setOnItemSelectedListener(this);

        return v;
    }

    //funtion นี้ ใช้กับ ปุ่ม Button ของ Light Air Fan ใน CardView
    public void getDataFilter(String typename, final String title) {
        callfillter = apiInterface.getData(typename);
        callfillter.enqueue(new Callback<List<AllDevice>>() {
            @Override
            public void onResponse(Call<List<AllDevice>> call, Response<List<AllDevice>> response) {
                if (response.isSuccessful()) {
                    list.addAll(response.body());
                }
                StringBuilder reset = new StringBuilder();
                String[] name = new String[list.size()];
                for ( int i = 0; i < list.size(); i++) {
                    try {
                        barEntries.add(new BarEntry(i, list.get(i).getTotal()));
                        name[i]=list.get(i).getDevice_name();
                        summary = summary+list.get(i).getTotal();


                        if (name[i]!=null){
                            Log.d(TAG, "onResponse (try): "+name[i]);
//                            if (name.length>=1){
                                Log.d(TAG, "barEntries : "+barEntries);
                                Log.d(TAG, "name : "+name);
                                Log.d(TAG, "barchart : "+barChart);
                                Log.d(TAG, "title : "+title);
                                Log.d(TAG, "list : "+list);
//                                setBarChart(barEntries,name,barChart,title);
//                            }
                        }
                    }catch (Exception e){
                        reset.setLength(0);
                        reset.append(name[i]);
                        list.clear();
                        barEntries.clear();
                        barChart.clear();
//                        for ( i=0;i<list.size();i++){
//                            barEntries.add(new BarEntry(i, list.get(i).getTotal()));
//                            name[i]=list.get(i).getDevice_name();
//                            Log.d(TAG, "onResponse (try): "+name[i]);
//                            Log.d(TAG, "barEntries : "+barEntries);
//                            Log.d(TAG, "name : "+name);
//                            Log.d(TAG, "barchart : "+barChart);
//                            Log.d(TAG, "title : "+title);
//                            Log.d(TAG, "list : "+list);
//                            setBarChart(barEntries,name,barChart,title);
//
//                        }
                        Log.d(TAG, "onResponse(Exception): "+name[i]);
                        Toast.makeText(getContext(), "กรุณาคลิก'แสดงกราฟ'ซ้ำอีกครั้ง", Toast.LENGTH_SHORT).show();
                    }
                    finally {
                        if (name[i]!=null){
                            Log.d(TAG, "onResponse(finally loop): "+name[i]);
                        }
                    }
                }
                setBarChart(barEntries,name,barChart,title);
                tv_desc_unit.setText("รวมหน่วยไฟฟ้าที่ใช้:" + summary+"หน่วย");
                Log.d(TAG, "Summary: "+summary);
            }
            @Override
            public void onFailure(Call<List<AllDevice>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        tv_datestart.setOnClickListener(this);
        tv_dateend.setOnClickListener(this);

        tv_month.setOnClickListener(this);
        tv_monthend.setOnClickListener(this);

        tv_year.setOnClickListener(this);
        tv_yearend.setOnClickListener(this);

        //filter Button
        btn_filter.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_datestart:
                dateStart(tv_datestart);
                break;
            case R.id.tv_dateend:
                dateEnd(tv_dateend);
                break;
            case R.id.tv_month:
                month(tv_month);
                break;
            case R.id.tv_monthend:
                monthend(tv_monthend);
                break;
            case R.id.tv_year:
                year(tv_year);
                break;
            case R.id.tv_yearend:
                yearend(tv_yearend);
                break;
            case R.id.btn_filter:
                //20201016 กดปุ่ม Button แสดงกราฟ ในส่วนของการกรอง ให้ Clear ค่า ทุกครั้ง
                list.clear();
                dayList.clear();
                monthList.clear();
                yearList.clear();
                barChart.clear();
                barEntries.clear();
                filter();
                break;
        }

    }

    //สร้าง funtion เพื่อเอาไปใช้งาน ตอนคลิก DatePicker
    String filtertitle;
    int datestart = 0;
    int dateend = 0;
    int month = 0;
    int year = 0;

    public void dateStart(final TextView v){ //ไม่ได้ใช้ library ใช้ของ android studio
        final Calendar calendar = Calendar.getInstance();
        int yearNow = calendar.get(Calendar.YEAR);
        int monthNow = calendar.get(Calendar.MONTH);
        final int dateNow = calendar.get(Calendar.DAY_OF_MONTH);
        Locale id = new Locale("in","ID");
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd",id);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year,month,dayOfMonth);
                tv_datestart.setText(simpleDateFormat.format(calendar.getTime()));
            }
        },yearNow,monthNow,dateNow);
        datePickerDialog.setTitle("เลือกวันเริ่มต้น");
        datePickerDialog.show();
    }

    public void dateEnd(final TextView v){
        final Calendar calendar = Calendar.getInstance();
        int yearNow = calendar.get(Calendar.YEAR);
        int monthNow = calendar.get(Calendar.MONTH);
        final int dateNow = calendar.get(Calendar.DAY_OF_MONTH);
        Locale id = new Locale("in","ID");
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd",id);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year,month,dayOfMonth);
                tv_dateend.setText(simpleDateFormat.format(calendar.getTime()));
            }
        },yearNow,monthNow,dateNow);
        datePickerDialog.setTitle("เลือกวันสิ้นสุด");
        datePickerDialog.show();
    }

    public void month(final TextView v) {
        final Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(getActivity(), new MonthPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int selectedMonth, int selectedYear) {
//                v.setText(""+(selectedMonth + 1));
                v.setText(getMonth(selectedMonth + 1));
                month = selectedMonth + 1;
            }
        },today.get(Calendar.YEAR),today.get(Calendar.MONTH));

        builder.showMonthOnly()
                .setTitle("ประจำเดือน")
                .setActivatedMonth(today.get(Calendar.MONTH))
                .build()
                .show();
    }

    private void monthend(final TextView v) {
        final Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(getActivity(), new MonthPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int selectedMonth, int selectedYear) {
//                v.setText(""+(selectedMonth + 1));
                v.setText(getMonth(selectedMonth + 1));
                month = selectedMonth + 1;
            }
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

        builder.showMonthOnly()
                .setTitle("")
                .setActivatedMonth(today.get(Calendar.MONTH))
                .build()
                .show();
    }

    public void year(final TextView v) {
        final Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(getActivity(), new MonthPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int selectedMonth, int selectedYear) {
                v.setText("" + selectedYear);
                year = selectedYear;
            }
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

        builder.showYearOnly()
                .setTitle("ประจำปี")
                .setYearRange(1990, today.get(Calendar.YEAR))
                .setActivatedYear(today.get(Calendar.YEAR))
                .build()
                .show();
    }

    private void yearend(final TextView v) {
        final Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(getActivity(), new MonthPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int selectedMonth, int selectedYear) {
                v.setText("" + selectedYear);
                year = selectedYear;
            }
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

        builder.showYearOnly()
                .setTitle("")
                .setYearRange(1990, today.get(Calendar.YEAR))
                .setActivatedYear(today.get(Calendar.YEAR))
                .build()
                .show();
    }
    //Button filter
    public void filter(){

        if ( devicename.trim().equals("เลือกประเภทอุปกรณ์") || filtertitle.trim().equals("เลือกประเภทรายงาน")){
            Toast.makeText(getContext(),"กรุณาเลือกประเภทอุปกรณ์และประเภทรายงาน",Toast.LENGTH_SHORT).show();
        }

        if (filtertitle.trim().equals("ตามช่วงวันที่")){
            barEntries.clear();
            dayList.clear();
            monthList.clear();
            yearList.clear();
            AlldayList.clear();
            AllmonthList.clear();
            AllyearList.clear();
            summary = 0;
            if(title.equals("all")){
                if (tv_datestart.getText().toString()!="วันเริ่มต้น" && tv_dateend.getText().toString()!="วันสิ้นสุด" && !filtertitle.trim().equals("เลือกประเภทรายงาน") && !devicename.trim().equals("เลือกประเภทอุปกรณ์")){
                    FilterAllByDay(tv_datestart.getText().toString(),tv_dateend.getText().toString(),"กราฟแยกตามช่วงเวลา");
//                    txtw.setText("รายงานทั้งหมด"+getTitle(title)+"จากวันที่"+tv_datestart.getText().toString()+" ถึงวันที่ "+tv_dateend.getText().toString());
                    tv_desc_dvname.setText("รายงานการใช้ไฟฟ้าของ:"+getTitle(title));
                    tv_desc_datetime.setText("ช่วงเวลา:"+tv_datestart.getText().toString()+" ถึง "+tv_dateend.getText().toString());

                    if (barChart.getVisibility()==View.GONE && tv_desc_dvname.getVisibility()==View.GONE && tv_desc_datetime.getVisibility()==View.GONE && tv_desc_unit.getVisibility()==View.GONE){
                        barChart.setVisibility(View.VISIBLE); //ซ่อนไว้
                        tv_desc_dvname.setVisibility(View.VISIBLE);
                        tv_desc_datetime.setVisibility(View.VISIBLE);
                        tv_desc_unit.setVisibility(View.VISIBLE);
                        tv_unit.setVisibility(View.VISIBLE);
                        tv_time.setVisibility(View.VISIBLE);
                    }else {
                        barChart.setVisibility(View.VISIBLE);
                        tv_desc_dvname.setVisibility(View.VISIBLE);
                        tv_desc_datetime.setVisibility(View.VISIBLE);
                        tv_desc_unit.setVisibility(View.VISIBLE);
                        tv_unit.setVisibility(View.VISIBLE);
                        tv_time.setVisibility(View.VISIBLE);
                    }
                }else {
                    Toast.makeText(getContext(),"กรุณาเลือกวันที่เริ่มต้น",Toast.LENGTH_SHORT).show();
                }
            }else {
                if (tv_datestart.getText().toString()!="วันเริ่มต้น" && tv_dateend.getText().toString()!="วันสิ้นสุด" && !filtertitle.trim().equals("เลือกประเภทรายงาน")&& !devicename.trim().equals("เลือกประเภทอุปกรณ์") ){
                    FilterByDay(title,tv_datestart.getText().toString(),tv_dateend.getText().toString(),"กราฟแยกตามช่วงเวลา");
//                    txtw.setText("รายงาน"+getTitle(title)+"จากวันที่"+tv_datestart.getText().toString()+" ถึงวันที่ "+tv_dateend.getText().toString());
                    tv_desc_dvname.setText("รายงานการใช้ไฟฟ้าของ:"+getTitle(title));
                    tv_desc_datetime.setText("ช่วงเวลา:"+tv_datestart.getText().toString()+" ถึง "+tv_dateend.getText().toString());
                    Log.d(TAG, "filter Day: "+tv_datestart+tv_dateend);

                    if (barChart.getVisibility()==View.GONE && tv_desc_dvname.getVisibility()==View.GONE && tv_desc_datetime.getVisibility()==View.GONE && tv_desc_unit.getVisibility()==View.GONE){
                        barChart.setVisibility(View.VISIBLE); //ซ่อนไว้
                        tv_desc_dvname.setVisibility(View.VISIBLE);
                        tv_desc_datetime.setVisibility(View.VISIBLE);
                        tv_desc_unit.setVisibility(View.VISIBLE);
                        tv_unit.setVisibility(View.VISIBLE);
                        tv_time.setVisibility(View.VISIBLE);

                    }else {
                        barChart.setVisibility(View.VISIBLE);
                        tv_desc_dvname.setVisibility(View.VISIBLE);
                        tv_desc_datetime.setVisibility(View.VISIBLE);
                        tv_desc_unit.setVisibility(View.VISIBLE);
                        tv_unit.setVisibility(View.VISIBLE);
                        tv_time.setVisibility(View.VISIBLE);
                    }
                }else {
                    Toast.makeText(getContext(),"กรุณาตรวจสอบข้อมูลอีกครั้ง",Toast.LENGTH_SHORT).show();
                }


                Log.d(TAG,"filter day"+tv_datestart+tv_dateend);
            }
        }
        if (filtertitle.trim().equals("รายเดือน")){
            barEntries.clear();
            dayList.clear();
            monthList.clear();
            yearList.clear();
            AlldayList.clear();
            AllmonthList.clear();
            AllyearList.clear();
            summary = 0;



            if(title.equals("all")){
                if (tv_month.getText().toString()!="เดือนเริ่มต้น" && tv_monthend.getText().toString()!="เดือนสิ้นสุด" && tv_year.getText().toString()!="ปีเริ่มต้น" && tv_yearend.getText().toString()!="ปีสิ้นสุด" && !filtertitle.trim().equals("เลือกประเภทรายงาน")&& !devicename.trim().equals("เลือกประเภทอุปกรณ์")){

//                   int monthstart  = getMonthnum(tv_month.getText().toString());
//                    int montend = getMonthnum(tv_monthend.getText().toString());
////                    int monthstart = Integer.parseInt(getMonthnum(tv_month.getText().toString()));
////                    int montend = Integer.parseInt(tv_monthend.getText().toString());
//                    int max = monthstart;
//                    int min = monthstart;
//                    if (montend > max){
//                        max = montend;
//                    }else if (montend<min){
//                        min = montend;
//                    };
//                    Log.d(TAG, "filter() returned min: " + min);
//                    Log.d(TAG, "filter() returned max: " + max);
//
//                    String monthmin = Integer.toString(min);
//                    String monthmax = Integer.toString(max);
                    String monthstart  = (tv_year.getText().toString())+getMonthnum(tv_month.getText().toString());
                    String monthend  = (tv_yearend.getText().toString())+getMonthnum(tv_monthend.getText().toString());

                    FilterAllByMonth(monthstart,monthend,"กราฟแยกตามช่วงเวลา");
//                    txtw.setText("รายงานทั้งหมด"+getTitle(title)+"เดือน"+tv_month.getText().toString()+"ปี "+tv_year.getText().toString());
                    tv_desc_dvname.setText("รายงานการใช้ไฟฟ้าของ:"+getTitle(title));
                    tv_desc_datetime.setText("ช่วงเวลาของเดือน: "+tv_month.getText().toString()+" ปี "+tv_year.getText().toString()+" ถึงเดือน "+tv_monthend.getText().toString()+" ปี "+tv_yearend.getText().toString());

                    if (barChart.getVisibility()==View.GONE && tv_desc_dvname.getVisibility()==View.GONE && tv_desc_datetime.getVisibility()==View.GONE && tv_desc_unit.getVisibility()==View.GONE){
                        barChart.setVisibility(View.VISIBLE); //ซ่อนไว้
                        tv_desc_dvname.setVisibility(View.VISIBLE);
                        tv_desc_datetime.setVisibility(View.VISIBLE);
                        tv_desc_unit.setVisibility(View.VISIBLE);
                        tv_unit.setVisibility(View.VISIBLE);
                        tv_time.setVisibility(View.VISIBLE);
                    }else {
                        barChart.setVisibility(View.VISIBLE);
                        tv_desc_dvname.setVisibility(View.VISIBLE);
                        tv_desc_datetime.setVisibility(View.VISIBLE);
                        tv_desc_unit.setVisibility(View.VISIBLE);
                        tv_unit.setVisibility(View.VISIBLE);
                        tv_time.setVisibility(View.VISIBLE);
                    }
                }else {
                    Toast.makeText(getContext(),"กรุณาเลือกเดือนและปี",Toast.LENGTH_SHORT).show();
                }

            }else {
                String monthstart  = (tv_year.getText().toString())+getMonthnum(tv_month.getText().toString());
                String monthend  = (tv_yearend.getText().toString())+getMonthnum(tv_monthend.getText().toString());
                if (tv_month.getText().toString()!="เดือนเริ่มต้น" && tv_monthend.getText().toString()!="เดือนสิ้นสุด" && tv_year.getText().toString()!="ปีเริ่มต้น" && tv_yearend.getText().toString()!="ปีสิ้นสุด" && !filtertitle.trim().equals("เลือกประเภทรายงาน")&& !devicename.trim().equals("เลือกประเภทอุปกรณ์")){
                    Log.d(TAG, "filter: "+monthstart +"lmonth"+monthend);
                    FilterByMonth(title,monthstart,monthend,"กราฟแยกตามเดือน");
//                    txtw.setText("รายงานการ"+getTitle(title)+"เดือนที่"+tv_month.getText().toString());
                    tv_desc_dvname.setText("รายงานการใช้ไฟฟ้าของ:"+getTitle(title));
                    tv_desc_datetime.setText("ช่วงเวลาของเดือน: "+tv_month.getText().toString()+" ปี  "+tv_year.getText().toString()+" ถึงเดือน "+tv_monthend.getText().toString()+" ปี " +tv_yearend.getText().toString());

                    if (barChart.getVisibility()==View.GONE && tv_desc_dvname.getVisibility()==View.GONE && tv_desc_datetime.getVisibility()==View.GONE && tv_desc_unit.getVisibility()==View.GONE){
                        barChart.setVisibility(View.VISIBLE); //ซ่อนไว้
                        tv_desc_dvname.setVisibility(View.VISIBLE);
                        tv_desc_datetime.setVisibility(View.VISIBLE);
                        tv_desc_unit.setVisibility(View.VISIBLE);
                        tv_unit.setVisibility(View.VISIBLE);
                        tv_time.setVisibility(View.VISIBLE);
                    }else {
                        barChart.setVisibility(View.VISIBLE);
                        tv_desc_dvname.setVisibility(View.VISIBLE);
                        tv_desc_datetime.setVisibility(View.VISIBLE);
                        tv_desc_unit.setVisibility(View.VISIBLE);
                        tv_unit.setVisibility(View.VISIBLE);
                        tv_time.setVisibility(View.VISIBLE);
                    }
                }else {
                    Toast.makeText(getContext(),"กรุณาเลือกเดือนและปี",Toast.LENGTH_SHORT).show();
                }

                Log.d(TAG,"filter month"+tv_year+tv_month);
            }
        }
        if(filtertitle.trim().equals("รายปี")){
            barEntries.clear();
            dayList.clear();
            monthList.clear();
            yearList.clear();
            AlldayList.clear();
            AllmonthList.clear();
            AllyearList.clear();
            summary = 0;
            if(title.equals("all")){
                if (tv_year.getText().toString()!="ปี" && !filtertitle.trim().equals("เลือกประเภทรายงาน") && !devicename.trim().equals("เลือกประเภทอุปกรณ์")){
                    FilterAllByYear(tv_year.getText().toString(),tv_yearend.getText().toString(),"กราฟแยกตามช่วงเวลา");
//                    txtw.setText("รายงาน"+getTitle(title)+"ของปี"+tv_year.getText().toString());
                    tv_desc_dvname.setText("รายงานการใช้ไฟฟ้าของ:"+getTitle(title));
                    tv_desc_datetime.setText("ช่วงเวลาของปี:"+tv_year.getText().toString()+"ถึงปี"+tv_yearend.getText().toString());

                    if (barChart.getVisibility()==View.GONE && tv_desc_dvname.getVisibility()==View.GONE && tv_desc_datetime.getVisibility()==View.GONE && tv_desc_unit.getVisibility()==View.GONE){
                        barChart.setVisibility(View.VISIBLE); //ซ่อนไว้
                        tv_desc_dvname.setVisibility(View.VISIBLE);
                        tv_desc_datetime.setVisibility(View.VISIBLE);
                        tv_desc_unit.setVisibility(View.VISIBLE);
                        tv_unit.setVisibility(View.VISIBLE);
                        tv_time.setVisibility(View.VISIBLE);
                    }else {
                        barChart.setVisibility(View.VISIBLE);
                        tv_desc_dvname.setVisibility(View.VISIBLE);
                        tv_desc_datetime.setVisibility(View.VISIBLE);
                        tv_desc_unit.setVisibility(View.VISIBLE);
                        tv_unit.setVisibility(View.VISIBLE);
                        tv_time.setVisibility(View.VISIBLE);
                    }
                }else {
                    Toast.makeText(getContext(),"กรุณาเลือกปี",Toast.LENGTH_SHORT).show();
                }

            }else {
                if (tv_year.getText().toString()!="ปีเริ่มต้น" && tv_yearend.getText()!="ปีสิ้นสุด" && !filtertitle.trim().equals("เลือกประเภทรายงาน") && !devicename.trim().equals("เลือกประเภทอุปกรณ์")){
                    FilterByYear(title,tv_year.getText().toString(),tv_yearend.getText().toString(),"กราฟแยกตามปี");
//                    txtw.setText("รายงานการใช้"+getTitle(title)+"ปีที่"+tv_year.getText().toString());
                    tv_desc_dvname.setText("รายงานการใช้ไฟฟ้าของ:"+getTitle(title));
                    tv_desc_datetime.setText("ช่วงเวลาของปี:"+tv_year.getText().toString()+" ถึงปี "+tv_yearend.getText().toString());

                    if (barChart.getVisibility()==View.GONE && tv_desc_dvname.getVisibility()==View.GONE && tv_desc_datetime.getVisibility()==View.GONE && tv_desc_unit.getVisibility()==View.GONE){
                        barChart.setVisibility(View.VISIBLE); //ซ่อนไว้
                        tv_desc_dvname.setVisibility(View.VISIBLE);
                        tv_desc_datetime.setVisibility(View.VISIBLE);
                        tv_desc_unit.setVisibility(View.VISIBLE);
                        tv_unit.setVisibility(View.VISIBLE);
                        tv_time.setVisibility(View.VISIBLE);
                    }else {
                        barChart.setVisibility(View.VISIBLE);
                        tv_desc_dvname.setVisibility(View.VISIBLE);
                        tv_desc_datetime.setVisibility(View.VISIBLE);
                        tv_desc_unit.setVisibility(View.VISIBLE);
                        tv_unit.setVisibility(View.VISIBLE);
                        tv_time.setVisibility(View.VISIBLE);
                    }
                }else {
                    Toast.makeText(getContext(),"กรุณาเลือกปี",Toast.LENGTH_SHORT).show();
                }

                Log.d(TAG,"filter year"+tv_year);
            }
        }
    }

    //spinner
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        filtertitle = parent.getSelectedItem().toString().trim();
        Log.d(TAG, "onItemSelected: "+filtertitle);

        tv_dateend.setText("วันสิ้นสุด");
        tv_datestart.setText("วันเริ่มต้น");
        tv_month.setText("เดือนเริ่มต้น");
        tv_monthend.setText("เดือนสิ้นสุด");
        tv_year.setText("ปีเริ่มต้น");
        tv_yearend.setText("ปีสิ้นสุด");
//        txtw.setText("");
        tv_desc_dvname.setText("");
        tv_desc_datetime.setText("");
        tv_desc_unit.setText("");

        if (filtertitle.trim().equals("เลือกประเภทรายงาน")) {
            hsv_tv.setVisibility(View.GONE);

            list.clear();
            dayList.clear();
            monthList.clear();
            yearList.clear();
            AlldayList.clear();
            AllmonthList.clear();
            AllyearList.clear();
            barChart.clear();
            barEntries.clear();

        } else if (filtertitle.trim().equals("ตามช่วงวันที่")) {
            list.clear();
            dayList.clear();
            monthList.clear();
            yearList.clear();
            AlldayList.clear();
            AllmonthList.clear();
            AllyearList.clear();
            barChart.clear();
            barEntries.clear();
            barChart.setVisibility(View.GONE); //ซ่อนกราฟ
            tv_desc_dvname.setVisibility(View.GONE);
            tv_desc_datetime.setVisibility(View.GONE);
            tv_desc_unit.setVisibility(View.GONE);
            tv_unit.setVisibility(View.GONE);
            tv_time.setVisibility(View.GONE);

            tv_datestart.setVisibility(View.VISIBLE);
            tv_dateend.setVisibility(View.VISIBLE);
            tv_month.setVisibility(View.GONE);
            tv_monthend.setVisibility(View.GONE);
            tv_year.setVisibility(View.GONE);
            tv_yearend.setVisibility(View.GONE);

            Log.d(TAG, "onItemSelected: clearDay");
            if (tv_datestart.isEnabled()&&tv_dateend.isEnabled()) {
                tv_month.setEnabled(false);
                tv_monthend.setEnabled(false);
                tv_year.setEnabled(false);
                tv_yearend.setEnabled(false);
                hsv_tv.setVisibility(view.VISIBLE);

            }else {
                tv_datestart.setEnabled(true);
                tv_dateend.setEnabled(true);
                tv_month.setEnabled(false);
                tv_monthend.setEnabled(false);
                tv_year.setEnabled(false);
                tv_yearend.setEnabled(false);
                hsv_tv.setVisibility(view.VISIBLE);
            }
        } else if (filtertitle.trim().equals("รายเดือน")){
            list.clear();
            dayList.clear();
            monthList.clear();
            yearList.clear();
            AlldayList.clear();
            AllmonthList.clear();
            AllyearList.clear();
            barChart.clear();
            barEntries.clear();
            barChart.setVisibility(View.GONE); //ซ่อนกราฟ
            tv_desc_dvname.setVisibility(View.GONE);
            tv_desc_datetime.setVisibility(View.GONE);
            tv_desc_unit.setVisibility(View.GONE);
            tv_unit.setVisibility(View.GONE);
            tv_time.setVisibility(View.GONE);

            tv_datestart.setVisibility(View.GONE);
            tv_dateend.setVisibility(View.GONE);
            tv_month.setVisibility(View.VISIBLE);
            tv_monthend.setVisibility(View.VISIBLE);
            tv_year.setVisibility(View.VISIBLE);
            tv_yearend.setVisibility(View.VISIBLE);

//            tv_year.setText("ปี");

            Log.d(TAG, "onItemSelected: clearMonth");

            if (tv_month.isEnabled() && tv_monthend.isEnabled() && tv_year.isEnabled()&& tv_yearend.isEnabled()){
                tv_datestart.setEnabled(false);
                tv_dateend.setEnabled(false);
//                tv_yearend.setEnabled(false);
                hsv_tv.setVisibility(view.VISIBLE);
            }
            else {
                tv_month.setEnabled(true);
                tv_monthend.setEnabled(true);
                tv_year.setEnabled(true);
                tv_datestart.setEnabled(false);
                tv_dateend.setEnabled(false);
                tv_yearend.setEnabled(true);
                hsv_tv.setVisibility(view.VISIBLE);

            }
        }else {
            list.clear();
            dayList.clear();
            monthList.clear();
            yearList.clear();
            AlldayList.clear();
            AllmonthList.clear();
            AllyearList.clear();
            barChart.clear();
            barEntries.clear();

            barChart.setVisibility(View.GONE); //ซ่อนกราฟ
            tv_desc_dvname.setVisibility(View.GONE);
            tv_desc_datetime.setVisibility(View.GONE);
            tv_desc_unit.setVisibility(View.GONE);
            tv_unit.setVisibility(View.GONE);
            tv_time.setVisibility(View.GONE);

            tv_datestart.setVisibility(View.GONE);
            tv_dateend.setVisibility(View.GONE);
            tv_month.setVisibility(View.GONE);
            tv_monthend.setVisibility(View.GONE);
            tv_year.setVisibility(View.VISIBLE);
            tv_yearend.setVisibility(View.VISIBLE);

            Log.d(TAG, "onItemSelected: Clear Year");

            tv_datestart.setEnabled(false);
            tv_dateend.setEnabled(false);
            tv_month.setEnabled(false);
            tv_monthend.setEnabled(false);
            tv_year.setEnabled(true);
            tv_yearend.setEnabled(true);
            hsv_tv.setVisibility(view.VISIBLE);


        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    //setBarChart
    String chartName;
    public void setBarChart(ArrayList<BarEntry> barEntries, final String[] name, BarChart chart, String title) {
        this.getTitle(title);
        final BarDataSet dataSet = new BarDataSet(barEntries,chartName);
        dataSet.setValueTextSize(12f);
        if (devicename.equals("หลอดไฟ")){
            dataSet.setColor(Color.BLACK);
        }else if (devicename.equals("แอร์")) {
            dataSet.setColor(Color.RED);
        }
        else if (devicename.equals("พัดลมระบายอากาศ")){
            dataSet.setColor(Color.GREEN);
        }
        else {
            dataSet.setColors(ColorTemplate.PASTEL_COLORS);
        }
        Log.d(TAG, "setCahrtname: "+devicename);


        dataSet.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {

                return  value + "";
            }
        });
        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(dataSet);
        Log.d(TAG, "setBarChart: dataSet"+dataSet);

        BarData data = new BarData(dataSets);
        chart.setData(data);
        chart.animateY(1000);
        chart.getDescription().setText("");
        chart.getLegend().setEnabled(false);
        chart.getAxisRight().setEnabled(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getAxisLeft().setAxisMinimum(0);
        chart.setDoubleTapToZoomEnabled(false);
        chart.setPinchZoom(false);
        chart.animateXY(3000,5000, Easing.EasingOption.EaseInCubic, Easing.EasingOption.EaseInBounce);
        final XAxis xAxis = chart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setTextSize(12f);

//        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                Log.d("benznest", "value = " + value);
                if (value < 0 || value >= name.length) {
                    return "";
                }
                return name[((int) value)];
            }
        });

    }

    //Filter By day month and year
    float summary = 0;
    public void FilterByDay(String typename,String p_first,String p_last, final String title){
        callDay = apiInterface.getDataByDay(uid,typename,p_first,p_last);
        callDay.enqueue(new Callback<List<FilterModel>>() {
            @Override
            public void onResponse(Call<List<FilterModel>> call, Response<List<FilterModel>> response) {
                if (response.isSuccessful()) {
                    dayList.addAll(response.body());
                }

                StringBuilder reset = new StringBuilder();
                String[] name = new String[dayList.size()];
                for (int i = 0; i < dayList.size(); i++) {
                    try {
                        barEntries.add(new BarEntry(i, dayList.get(i).getTotal()));
                        name[i]=dayList.get(i).getDay();
                        summary = summary+dayList.get(i).getTotal();
                        if (name[i]!=null){
                            Log.d(TAG, "onResponse (try): "+name[i]);
//                            if (name.length>=1){
                            Log.d(TAG, "barEntries : "+barEntries);
                            Log.d(TAG, "name : "+name);
                            Log.d(TAG, "barchart : "+barChart);
                            Log.d(TAG, "title : "+title);
                            Log.d(TAG, "dayList : "+dayList);
                            setBarChart(barEntries,name,barChart,title);
//                            }
                        }
                    }catch (Exception e){
                        reset.setLength(0);
                        reset.append(name[i]);
                        dayList.clear();
                        barEntries.clear();
                        barChart.clear();
                        Log.d(TAG, "onResponse(Exception): "+name[i]);
                        Toast.makeText(getContext(), "กรุณาคลิก'แสดงกราฟ'ซ้ำอีกครั้ง", Toast.LENGTH_SHORT).show();
                        setBarChart(barEntries,name,barChart,title);
                    }
                    finally {
                        if (name[i]!=null){
                            Log.d(TAG, "onResponse(finally loop): "+name[i]);
                        }
                    }
                }
                Log.d(TAG, "Summary: "+summary);
                tv_desc_unit.setText("รวมหน่วยไฟฟ้าที่ใช้:" + summary+"หน่วย");
            }

            @Override
            public void onFailure(Call<List<FilterModel>> call, Throwable t) {
                Log.d(TAG, "onFailure: filterDay");
            }
        });
    }

    public void FilterByMonth(String typename,String f_month,String l_month, final String title){
        callMonth = apiInterface.getDataByMonth(uid,typename,f_month,l_month);
        callMonth.enqueue(new Callback<List<FilterModel>>() {
            @Override
            public void onResponse(Call<List<FilterModel>> call, Response<List<FilterModel>> response) {
                if (response.isSuccessful()) {
                    monthList.addAll(response.body());
                }

                StringBuilder reset = new StringBuilder();
                String[] name = new String[monthList.size()];

                for (int i = 0; i < monthList.size(); i++) {
                    try {
                        barEntries.add(new BarEntry(i, monthList.get(i).getTotal()));
                        name[i] = monthList.get(i).getDay();
//                        name[i] = String.valueOf(monthList.get(i).getDay());
                        summary = summary + monthList.get(i).getTotal();

                        if (name[i]!=null){
                            Log.d(TAG, "onResponse month: "+name[i]);
                            Log.d(TAG, "barEntries : "+barEntries);
                            Log.d(TAG, "name : "+name);
                            Log.d(TAG, "barchart : "+barChart);
                            Log.d(TAG, "title : "+title);
                            Log.d(TAG, "monthList : "+monthList);
                            setBarChart(barEntries,name,barChart,title);
                        }

                    }catch (Exception e){
                        reset.setLength(0);
                        reset.append(name[i]);
                        monthList.clear();
                        barChart.clear();
                        barEntries.clear();
                        Log.d(TAG, "FilterMonth onResponse Exception : "+name[i]);
                        Toast.makeText(getContext(), "กรุณาคลิก'แสดงกราฟ'ซ้ำอีกครั้ง", Toast.LENGTH_SHORT).show();
                        setBarChart(barEntries, name, barChart, title);
                    }
                    finally {
                        if (name[i]!=null) {
                            Log.d(TAG, "FilterMonth onResponse finally: "+name[i]);
                        }
                    }
                }
                tv_desc_unit.setText("รวมหน่วยไฟฟ้าที่ใช้:" + summary+"หน่วย");
            }

            @Override
            public void onFailure(Call<List<FilterModel>> call, Throwable t) {
                Log.d(TAG, "onFailure: filterMonth");
            }
        });
    }

    public void FilterByYear(String typename ,String f_year,String l_year, final String title){
        callYear = apiInterface.getDataByYear(uid,typename,f_year,l_year);
        callYear.enqueue(new Callback<List<FilterModel>>() {
            @Override
            public void onResponse(Call<List<FilterModel>> call, Response<List<FilterModel>> response) {
                if (response.isSuccessful()) {
                    yearList.addAll(response.body());
                }
                StringBuilder reset = new StringBuilder();
                String[] name = new String[yearList.size()];
                for (int i = 0; i < yearList.size(); i++) {
                    try {
                        barEntries.add(new BarEntry(i, yearList.get(i).getTotal()));
                        name[i] = yearList.get(i).getDay();
                        summary = summary + yearList.get(i).getTotal();

                        if (name[i]!=null){
                            Log.d(TAG, "onResponse FilterByYear: "+name[i]);
                            Log.d(TAG, "barEntries : "+barEntries);
                            Log.d(TAG, "name : "+name);
                            Log.d(TAG, "barchart : "+barChart);
                            Log.d(TAG, "title : "+title);
                            Log.d(TAG, "yearList : "+yearList);
                            setBarChart(barEntries,name,barChart,title);
                        }

                    }catch (Exception e){
                        reset.setLength(0);
                        reset.append(name[i]);
                        yearList.clear();
                        barChart.clear();
                        barEntries.clear();
                        Log.d(TAG, "FilterYera onResponse Exception : "+name[i]);
                        Toast.makeText(getContext(), "กรุณาคลิก'แสดงกราฟ'ซ้ำอีกครั้ง", Toast.LENGTH_SHORT).show();
                        setBarChart(barEntries, name, barChart, title);
                    }
                    finally {

                        if (name[i]!=null) {
                            Log.d(TAG, "FilterYera onResponse finally: "+name[i]);
                        }
                    }
                }
                tv_desc_unit.setText("รวมหน่วยไฟฟ้าที่ใช้:" + summary+"หน่วย");
            }

            @Override
            public void onFailure(Call<List<FilterModel>> call, Throwable t) {
                Log.d(TAG, "onFailure: filterYear");
            }
        });
    }

    //FilterAllDevice By day month and year
    public void FilterAllByDay(String p_first,String p_last, final String title){
        callAllDay = apiInterface.getDataAllByDay(uid,p_first,p_last);
        callAllDay.enqueue(new Callback<List<FilterModel>>() {
            @Override
            public void onResponse(Call<List<FilterModel>> call, Response<List<FilterModel>> response) {
                if (response.isSuccessful()) {
                    AlldayList.addAll(response.body());
                }
                StringBuilder reset = new StringBuilder();
                String[] name = new String[AlldayList.size()];
                for (int i = 0; i < AlldayList.size(); i++) {
                    try {
                        barEntries.add(new BarEntry(i, AlldayList.get(i).getTotal()));
                        name[i]=AlldayList.get(i).getDay();
                        summary = summary + AlldayList.get(i).getTotal();

                        if (name[i]!=null){
                            Log.d(TAG, "onResponse (try): "+name[i]);
                            if (name.length>=1){
                            Log.d(TAG, "barEntries : "+barEntries);
                            Log.d(TAG, "name : "+name);
                            Log.d(TAG, "barchart : "+barChart);
                            Log.d(TAG, "title : "+title);
                            Log.d(TAG, "AlldayList : "+AlldayList);
//                            setBarChart(barEntries,name,barChart,title);
                            }
                        }
                    }catch (Exception e){
                        reset.setLength(0);
                        reset.append(name[i]);
                        AlldayList.clear();
                        barEntries.clear();
                        barChart.clear();
                        Log.d(TAG, "onResponse(Exception): "+name[i]);
                        Toast.makeText(getContext(), "กรุณาคลิก'แสดงกราฟ'ซ้ำอีกครั้ง", Toast.LENGTH_SHORT).show();
                    }
                    finally {
                        if (name[i]!=null){
                            Log.d(TAG, "onResponse(finally loop): "+name[i]);
                            Log.d(TAG, "onResponse(finally neme.leghth): "+name.length);

                        }
                    }
                }
                if (name.length>0) {
                    setBarChart(barEntries, name, barChart, title);
                    tv_desc_unit.setText("รวมหน่วยไฟฟ้าที่ใช้:" + summary+"หน่วย");
                }
            }

            @Override
            public void onFailure(Call<List<FilterModel>> call, Throwable t) {
                Log.d(TAG, "onFailure: ");

            }
        });
    }

    public void FilterAllByMonth(String f_month,String l_month, final String title){
        callAllMonth = apiInterface.getDataAllByMonth(uid,f_month,l_month);
        callAllMonth.enqueue(new Callback<List<FilterModel>>() {
            @Override
            public void onResponse(Call<List<FilterModel>> call, Response<List<FilterModel>> response) {
                if (response.isSuccessful()) {
                    AllmonthList.addAll(response.body());
                }
                StringBuilder reset = new StringBuilder();
                String[] name = new String[AllmonthList.size()];
                for (int i = 0; i < AllmonthList.size(); i++) {
                    try {
                        barEntries.add(new BarEntry(i, AllmonthList.get(i).getTotal()));
                        name[i]=AllmonthList.get(i).getDay();
                        summary = summary + AllmonthList.get(i).getTotal();

                        if (name[i]!=null){
                            Log.d(TAG, "onResponse (try): "+name[i]);
                            if (name.length>0){
                                Log.d(TAG, "barEntries : "+barEntries);
                                Log.d(TAG, "name : "+name);
                                Log.d(TAG, "barchart : "+barChart);
                                Log.d(TAG, "title : "+title);
                                Log.d(TAG, "AllmonthList : "+AllmonthList);
//                            setBarChart(barEntries,name,barChart,title);
                            }
                        }
                    }catch (Exception e){
                        reset.setLength(0);
                        reset.append(name[i]);
                        AllmonthList.clear();
                        barEntries.clear();
                        barChart.clear();
                        Log.d(TAG, "onResponse(Exception): "+name[i]);
                        Toast.makeText(getContext(), "กรุณาคลิก'แสดงกราฟ'ซ้ำอีกครั้ง", Toast.LENGTH_SHORT).show();
                    }
                    finally {
                        if (name[i]!=null){
                            Log.d(TAG, "onResponse(finally loop): "+name[i]);
                            Log.d(TAG, "onResponse(finally neme.leghth): "+name.length);

                        }
                    }
                }
                if (name.length>0) {
                    setBarChart(barEntries, name, barChart, title);
                    tv_desc_unit.setText("รวมหน่วยไฟฟ้าที่ใช้:" +summary+"หน่วย");
                }
            }

            @Override
            public void onFailure(Call<List<FilterModel>> call, Throwable t) {
                Log.d(TAG, "onFailure: ");

            }
        });
    }

    public void FilterAllByYear(String f_year,String l_year, final String title){
        callAllYear = apiInterface.getDataAllByYear(uid,f_year,l_year);
        callAllYear.enqueue(new Callback<List<FilterModel>>() {
            @Override
            public void onResponse(Call<List<FilterModel>> call, Response<List<FilterModel>> response) {
                if (response.isSuccessful()) {
                    AllyearList.addAll(response.body());
                }
                StringBuilder reset = new StringBuilder();
                String[] name = new String[AllyearList.size()];
                for (int i = 0; i < AllyearList.size(); i++) {
                    try {
                        barEntries.add(new BarEntry(i, AllyearList.get(i).getTotal()));
                        name[i]=AllyearList.get(i).getDay();
                        summary = summary+AllyearList.get(i).getTotal();

                        if (name[i]!=null){
                            Log.d(TAG, "onResponse (try): "+name[i]);
                            if (name.length>0){
                                Log.d(TAG, "barEntries : "+barEntries);
                                Log.d(TAG, "name : "+name);
                                Log.d(TAG, "barchart : "+barChart);
                                Log.d(TAG, "title : "+title);
                                Log.d(TAG, "AllyearList : "+AllyearList);
                            setBarChart(barEntries,name,barChart,title);
                            }
                        }
                    }catch (Exception e){
                        reset.setLength(0);
                        reset.append(name[i]);
                        AllyearList.clear();
                        barEntries.clear();
                        barChart.clear();
                        Log.d(TAG, "onResponse(Exception): "+name[i]);
                        Toast.makeText(getContext(), "กรุณาคลิก'แสดงกราฟ'ซ้ำอีกครั้ง", Toast.LENGTH_SHORT).show();
                    }
                    finally {
                        if (name[i]!=null){
                            Log.d(TAG, "onResponse(finally loop): "+name[i]);
                            Log.d(TAG, "onResponse(finally neme.leghth): "+name.length);

                        }
                    }
                }
                tv_desc_unit.setText("รวมหน่วยไฟฟ้าที่ใช้:" +summary+"หน่วย");
            }

            @Override
            public void onFailure(Call<List<FilterModel>> call, Throwable t) {
                Log.d(TAG, "onFailure: ");

            }
        });
    }

    public String getTitle(String title){
        switch (title){
            case "light":
                return "หลอดไฟ";
            case "air":
                return "แอร์";
            case "fan":
                return"พัดลมระบายอากาศ";
            case"all":
                return"รวมทุกอุปกรณ์";
            default:
                return null;
        }
    }

    public String getMonth(int thMonth){
        switch (thMonth){
            case 1:
                return "ม.ค.";
            case 2:
                return "ก.พ.";
            case 3:
                return"มี.ค.";
            case 4:
                return"เม.ย.";
            case 5:
                return"พ.ค";
            case 6:
                return"มิ.ย.";
            case 7:
                return"ก.ค.";
            case 8:
                return"ส.ค";
            case 9:
                return"ก.ย.";
            case 10:
                return"ต.ค.";
            case 11:
                return"พ.ย.";
            case 12:
                return"ธ.ค.";
            default:
                break;
        }
        return null;
    }
    public String getMonthnum(String MonthNum){
        switch (MonthNum){
            case "ม.ค.":
                return "01";
            case "ก.พ.":
                return "02";
            case "มี.ค.":
                return "03";
            case "เม.ย.":
                return "04";
            case "พ.ค":
                return "05";
            case "มิ.ย.":
                return "06";
            case "ก.ค.":
                return "07";
            case "ส.ค":
                return "08";
            case "ก.ย.":
                return "09";
            case "ต.ค.":
                return "10";
            case "พ.ย.":
                return "11";
            case "ธ.ค.":
                return "12";
            default:
                break;
        }
        return null;
    }

}
