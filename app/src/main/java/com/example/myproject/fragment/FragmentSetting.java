package com.example.myproject.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myproject.Model.AirModel;
import com.example.myproject.Model.FanModel;
import com.example.myproject.Model.LightModel;
import com.example.myproject.Model.User;
import com.example.myproject.R;
import com.example.myproject.databinding.FragmentSettingBinding;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pixplicity.easyprefs.library.Prefs;


public class FragmentSetting extends Fragment {

    CardView cv_user, cv_device;
    TextView tv_email, tv_password;
    ConstraintLayout expanview_user, expanview_setdevice;
    Button btnv_user, btnv_close, btn_change, btnv_opendevice, btnv_closedevice,btn_logout;


    DatabaseReference RefLight,RefLightWatt, RefAir,RefAirWatt, RefFan,RefFanWatt;
    RecyclerView recyclerViewLight, recyclerViewAir, recyclerViewFan;

    FirebaseRecyclerOptions<LightModel> optionsLight;
    FirebaseRecyclerOptions<AirModel> optionsAir;
    FirebaseRecyclerOptions<FanModel> optionsFan;

    FirebaseRecyclerAdapter<LightModel, LightViewHolder> adapterLight;
    FirebaseRecyclerAdapter<AirModel, AirViewHolder> adapterAir;
    FirebaseRecyclerAdapter<FanModel, FanViewHolder> adapterFan;

    FragmentSettingBinding settingBinding;
    private FirebaseAuth mAuth;
    String Uid;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        settingBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false);
        View v = settingBinding.getRoot();
        //View v =inflater.inflate(R.layout.fragment_setting, container, false);

        cv_user = v.findViewById(R.id.cv_user);
        tv_email = v.findViewById(R.id.edt_email);
        tv_password = v.findViewById(R.id.edt_password);
        expanview_user = v.findViewById(R.id.expanview_user);
        btnv_user = v.findViewById(R.id.btnv_user);
        btnv_close = v.findViewById(R.id.btnv_close);
//        btn_change = v.findViewById(R.id.btn_change);
        btn_logout = v.findViewById(R.id.btn_logout);

        btnv_opendevice = v.findViewById(R.id.btnv_openDevice);
        btnv_closedevice = v.findViewById(R.id.btnv_closeDevice);
        expanview_setdevice = v.findViewById(R.id.expanview_setdevice);
        cv_device = v.findViewById(R.id.cv_device);

        Uid= FirebaseAuth.getInstance().getCurrentUser().getUid();

//        mAuth = FirebaseAuth.getInstance();
//        if (mAuth != null) {
            tv_email.setText(Prefs.getString("Id", "null"));
//            tv_password.setText(mAuth.getCurrentUser().getUid());
//        }

        //CardView EditUser DropdownButton ตั้งค่าข้อมูลสวนตัว
        btnv_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expanview_user.getVisibility() == View.GONE) {
                    TransitionManager.beginDelayedTransition(cv_user, new AutoTransition());
                    expanview_user.setVisibility(View.VISIBLE);
                    btnv_close.setVisibility(View.VISIBLE);
                } else {
                    TransitionManager.beginDelayedTransition(cv_user, new AutoTransition());
                    expanview_user.setVisibility(View.GONE);

                }
            }
        });
        btnv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expanview_user.getVisibility() == View.VISIBLE) {
                    expanview_user.setVisibility(View.GONE);
                    btnv_user.setVisibility(View.VISIBLE);
                    btnv_close.setVisibility(View.GONE);
                }

            }
        });
        // End CardView EditUser DropdownButton

        //Query Data in Firebase show in cardview
//        databaseReference=FirebaseDatabase.getInstance().getReference().child("User").child("fxBooQYUxIdD2MgDBh9QcwojpCp1");
//
//        databaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                String email = dataSnapshot.child("Email").getValue().toString();
//                String password = dataSnapshot.child("Password").getValue().toString();
//                edt_email.setText(email);
//                edt_password.setText(password);
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
        //End Query Data in Firebase show in cardview

//------------------------------- ส่วนของ ตั้งค่าอุปกรณ์ ------------------------------------//
        //SetDevice DropDown Button ปุ่ม ตั้งค่าอุปกรณ์ หลัก Main
        settingBinding.cvDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expanview_setdevice.getVisibility() == View.GONE) {
                    expanview_setdevice.setVisibility(View.VISIBLE);
                    btnv_closedevice.setVisibility(View.VISIBLE);
                    btnv_opendevice.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(cv_device, new AutoTransition());
                } else {
                    expanview_setdevice.setVisibility(View.GONE);
                    btnv_closedevice.setVisibility(View.GONE);
                    btnv_opendevice.setVisibility(View.VISIBLE);
                }
            }
        });
        btnv_opendevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expanview_setdevice.getVisibility() == View.GONE) {
                    TransitionManager.beginDelayedTransition(cv_device, new AutoTransition());
                    expanview_setdevice.setVisibility(View.VISIBLE);
                    btnv_closedevice.setVisibility(View.VISIBLE);
                    btnv_opendevice.setVisibility(View.GONE);
                } else {
                    expanview_setdevice.setVisibility(View.GONE);
                }
            }
        });

        btnv_closedevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expanview_setdevice.getVisibility() == View.VISIBLE) {
                    expanview_setdevice.setVisibility(View.GONE);
                    btnv_closedevice.setVisibility(View.GONE);
                    btnv_opendevice.setVisibility(View.VISIBLE);

                }
            }
        });
        //End SetDevice DropDown Button

        //---------Light-------SetLightName //Settings ดึงข้อมูลจาก FireBase โดยใช้ RecycleView แสดง
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RefLight = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("lightModel");

        optionsLight = new FirebaseRecyclerOptions.Builder<LightModel>()
                .setQuery(RefLight, LightModel.class).build();
        recyclerViewLight = v.findViewById(R.id.recycle_light);
        recyclerViewLight.setLayoutManager(layoutManager);

        adapterLight = new FirebaseRecyclerAdapter<LightModel, LightViewHolder>(optionsLight) {
            @Override
            protected void onBindViewHolder(@NonNull final LightViewHolder holder, int position, @NonNull final LightModel model) {
                holder.edt_name.setText(model.getName());
                holder.edt_watt.setText(""+model.getWatt());

                holder.btn_edit_light.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (holder.btn_save_light.getVisibility() == View.GONE) {
                            holder.btn_save_light.setVisibility(View.VISIBLE);
                            holder.btn_edit_light.setVisibility(View.GONE);

                            Toast.makeText(getContext(),"สามารถแก้ไขชื่อและกำลังวัตต์ได้แล้ว",Toast.LENGTH_LONG).show();

                            holder.edt_name.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    v.setFocusable(true);
                                    v.setFocusableInTouchMode(true);
                                    return false;
                                }
                            });
                            holder.edt_watt.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    v.setFocusable(true);
                                    v.setFocusableInTouchMode(true);
                                    return false;
                                }
                            });
                        } else {
                            holder.btn_save_light.setVisibility(View.GONE);
                        }
                    }
                });

                holder.btn_save_light.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        RefLight = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("lightModel").child(model.getId()).child("name");
                        RefLightWatt = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("lightModel").child(model.getId()).child("watt");

                        String edtname = holder.edt_name.getText().toString();
                        String edtwatt = holder.edt_watt.getText().toString();
                        int finalwatt = Integer.parseInt(edtwatt);

                        if (TextUtils.isEmpty(edtname)) {
                            holder.btn_save_light.setVisibility(View.VISIBLE);
                        } else {
                            holder.btn_save_light.setVisibility(View.GONE);
                            holder.btn_edit_light.setVisibility(View.VISIBLE);

                            holder.edt_name.setFocusable(false);
                            holder.edt_watt.setFocusable(false);

                            RefLight.setValue(holder.edt_name.getText().toString());
                            RefLightWatt.setValue(finalwatt);

                            Toast.makeText(getActivity(), "บันทึกแล้ว:)", Toast.LENGTH_LONG).show();

                            holder.edt_name.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    v.setFocusable(false);
                                    v.setFocusableInTouchMode(false);
//                                    Toast.makeText(getActivity(),"กรุณาตั้งชื่อให้หลอดไฟ!!!",Toast.LENGTH_LONG).show();
                                }
                            });
                            holder.edt_watt.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    v.setFocusable(false);
                                    v.setFocusableInTouchMode(false);
                                }
                            });
                        }
                    }
                });
                //TouchLight alert  กรุณากดปุ่มแก้ไข
                holder.edt_name.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_UP){
                            Toast.makeText(getContext(), "กรุณากดปุ่มแก้ไข!", Toast.LENGTH_SHORT).show();

                        }
                        return false;
                    }
                });
                holder.edt_watt.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction()== MotionEvent.ACTION_UP){
                            Toast.makeText(getContext(), "กรุณากดปุ่มแก้ไข!", Toast.LENGTH_SHORT).show();

                        }
                        return false;
                    }
                });

            }

            @NonNull
            @Override
            public LightViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.set_item_light, parent, false);
                return new LightViewHolder(view);
            }
        };
        // Show amount Light in descLight นับจำนวนของ item จะแสดงเมื่อมีการเชื่อต่อเน็ตแล้วดึงข้อมูลมาให้เห็น
        RefLight.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final int countLight = (int) dataSnapshot.getChildrenCount();
                settingBinding.descLight.setText("หลอดไฟทั้งหมด:" + countLight);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        // End Show amount Light in descLight จะแสดงเมื่อมีการเชื่อต่อเน็ตแล้วดึงข้อมูลมาให้เห็น
        adapterLight.startListening();
        recyclerViewLight.setAdapter(adapterLight);

        settingBinding.cvLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settingBinding.recycleLight.getVisibility() == View.GONE) {
                    settingBinding.recycleLight.setVisibility(View.VISIBLE);
                    settingBinding.arrowBtnLight.setVisibility(View.VISIBLE);
                    settingBinding.nextarrowBtnLight.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(settingBinding.cvLight, new AutoTransition());
                } else {
                    settingBinding.recycleLight.setVisibility(View.GONE);
                    settingBinding.nextarrowBtnLight.setVisibility(View.VISIBLE);
                    settingBinding.arrowBtnLight.setVisibility(View.GONE);
                }
            }
        });
        settingBinding.nextarrowBtnLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settingBinding.recycleLight.getVisibility() == View.GONE) {
                    settingBinding.recycleLight.setVisibility(View.VISIBLE);
                    settingBinding.arrowBtnLight.setVisibility(View.VISIBLE);
                    settingBinding.nextarrowBtnLight.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(settingBinding.cvLight, new AutoTransition());
                } else {
                    settingBinding.recycleLight.setVisibility(View.GONE);
                }
            }
        });
        settingBinding.arrowBtnLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settingBinding.recycleLight.getVisibility() == View.VISIBLE) {
                    settingBinding.recycleLight.setVisibility(View.GONE);
                    settingBinding.arrowBtnLight.setVisibility(View.GONE);
                    settingBinding.nextarrowBtnLight.setVisibility(View.VISIBLE);
                }
            }
        });
        //---------Air-------SetAirName //Settings ดึงข้อมูลจาก FireBase โดยใช้ RecycleView แสดง
        LinearLayoutManager layoutManagerAir = new LinearLayoutManager(getContext());
        RefAir = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("airModel");

        optionsAir = new FirebaseRecyclerOptions.Builder<AirModel>()
                .setQuery(RefAir, AirModel.class).build();
        recyclerViewAir = v.findViewById(R.id.recycle_air);
        recyclerViewAir.setLayoutManager(layoutManagerAir);

        adapterAir = new FirebaseRecyclerAdapter<AirModel, AirViewHolder>(optionsAir) {
            @Override
            protected void onBindViewHolder(@NonNull final AirViewHolder holder, int position, @NonNull final AirModel modelAir) {
                holder.edt_name_air.setText(modelAir.getName());
                holder.edt_watt_air.setText(""+modelAir.getWatt());

                holder.btn_edit_air.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (holder.btn_save_air.getVisibility() == View.GONE) {
                            holder.btn_save_air.setVisibility(View.VISIBLE);
                            holder.btn_edit_air.setVisibility(View.GONE);
                            Toast.makeText(getContext(),"สามารถแก้ไขได้แล้ว!",Toast.LENGTH_LONG).show();

                            holder.edt_name_air.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    v.setFocusable(true);
                                    v.setFocusableInTouchMode(true);
                                    return false;
                                }
                            });
                            holder.edt_watt_air.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    v.setFocusable(true);
                                    v.setFocusableInTouchMode(true);
                                    return false;
                                }
                            });
                        } else {
                            holder.btn_save_air.setVisibility(View.GONE);

                        }
                    }
                });

                holder.btn_save_air.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        RefAir = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("airModel").child(modelAir.getId()).child("name");
                        RefAirWatt = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("airModel").child(modelAir.getId()).child("watt");

                        String edtnameair = holder.edt_name_air.getText().toString();
                        String edtwattair = holder.edt_watt_air.getText().toString();

                        int finalwattair = Integer.parseInt(edtwattair);
                        if (TextUtils.isEmpty(edtnameair)) {
                            holder.btn_save_air.setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(), "กรุณาตั้งชื่อให้แอร์ไฟ!!!", Toast.LENGTH_LONG).show();
                        } else {
                            holder.btn_save_air.setVisibility(View.GONE);
                            holder.btn_edit_air.setVisibility(View.VISIBLE);
                            holder.edt_name_air.setFocusable(false);

                            RefAir.setValue(holder.edt_name_air.getText().toString());
                            RefAirWatt.setValue(finalwattair);

                            Toast.makeText(getActivity(), "บันทึกแล้ว:)", Toast.LENGTH_LONG).show();
                            holder.edt_name_air.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    v.setFocusable(false);
                                    v.setFocusableInTouchMode(false);
                                    return false;
                                }
                            });

                            holder.edt_watt_air.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                   v.setFocusable(false);
                                   v.setFocusableInTouchMode(false);
                                    return false;
                                }
                            });
                        }
                    }
                });
                //Touch Air alert  กรุณากดปุ่มแก้ไข
                holder.edt_name_air.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            Toast.makeText(getContext(), "กรุณากดปุ่มแก้ไข!", Toast.LENGTH_LONG).show();
                        }
                        return false;
                    }
                });

                holder.edt_watt_air.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                       if (event.getAction()== MotionEvent.ACTION_UP){
                           Toast.makeText(getContext(),"กรุณากดปุ่มแก้ไข!",Toast.LENGTH_SHORT).show();
                       }
                        return false;
                    }
                });
            }

            @NonNull
            @Override
            public AirViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.set_item_air, parent, false);
                return new AirViewHolder(view);
            }
        };
        // Show amount Air in descLight นับจำนวนของ item จะแสดงเมื่อมีการเชื่อต่อเน็ตแล้วดึงข้อมูลมาให้เห็น
        RefAir.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final int countAir = (int) dataSnapshot.getChildrenCount();
                settingBinding.descAir.setText("แอร์ทั้งหมด:" + countAir);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        // End Show amount Air in descLight จะแสดงเมื่อมีการเชื่อต่อเน็ตแล้วดึงข้อมูลมาให้เห็น
        adapterAir.startListening();
        recyclerViewAir.setAdapter(adapterAir);

        settingBinding.cvAir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settingBinding.recycleAir.getVisibility() == View.GONE) {
                    settingBinding.recycleAir.setVisibility(View.VISIBLE);
                    settingBinding.arrowBtnAir.setVisibility(View.VISIBLE);
                    settingBinding.nextarrowBtnAir.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(settingBinding.cvAir, new AutoTransition());
                } else {
                    settingBinding.recycleAir.setVisibility(View.GONE);
                    settingBinding.nextarrowBtnAir.setVisibility(View.VISIBLE);
                    settingBinding.arrowBtnAir.setVisibility(View.GONE);
                }
            }
        });
        settingBinding.nextarrowBtnAir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settingBinding.recycleAir.getVisibility() == View.GONE) {
                    settingBinding.recycleAir.setVisibility(View.VISIBLE);
                    settingBinding.arrowBtnAir.setVisibility(View.VISIBLE);
                    settingBinding.nextarrowBtnAir.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(settingBinding.cvAir, new AutoTransition());
                } else {
                    settingBinding.recycleAir.setVisibility(View.GONE);
                }
            }
        });
        settingBinding.arrowBtnAir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settingBinding.recycleAir.getVisibility() == View.VISIBLE) {
                    settingBinding.recycleAir.setVisibility(View.GONE);
                    settingBinding.arrowBtnAir.setVisibility(View.GONE);
                    settingBinding.nextarrowBtnAir.setVisibility(View.VISIBLE);
                }
            }
        });

//------Start Fan-------SetFanName //Settings ดึงข้อมูลจาก FireBase โดยใช้ RecycleView แสดง
        LinearLayoutManager layoutManagerFan = new LinearLayoutManager(getContext());
        RefFan = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("fanModel");

        optionsFan = new FirebaseRecyclerOptions.Builder<FanModel>()
                .setQuery(RefFan, FanModel.class).build();
        recyclerViewFan = v.findViewById(R.id.recycle_fan);
        recyclerViewFan.setLayoutManager(layoutManagerFan);

        adapterFan = new FirebaseRecyclerAdapter<FanModel, FanViewHolder>(optionsFan) {
            @Override
            protected void onBindViewHolder(@NonNull final FanViewHolder holder, int position, @NonNull final FanModel modelFan) {
                holder.edt_name_fan.setText(modelFan.getName());
                holder.edt_watt_fan.setText(""+modelFan.getWatt()+"วัตต์");//20200930เพิ่ม
                holder.btn_edit_Fan.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (holder.btn_save_Fan.getVisibility() == View.GONE) {
                            holder.btn_save_Fan.setVisibility(View.VISIBLE);
                           holder.btn_edit_Fan.setVisibility(View.GONE);
                            Toast.makeText(getContext(),"สามารถแก้ไขชื่อและกำลังวัตต์ได้แล้ว",Toast.LENGTH_LONG).show();

                            holder.edt_name_fan.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    v.setFocusable(true);
                                    v.setFocusableInTouchMode(true);
                                    return false;
                                }
                            });
                            //20200930เพิ่ม
                            holder.edt_watt_fan.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    v.setFocusable(true);
                                    v.setFocusableInTouchMode(true);
                                    return false;
                                }
                            });
                        } else {
                            holder.btn_save_Fan.setVisibility(View.GONE);

                        }
                    }
                });

                holder.btn_save_Fan.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        RefFan = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("fanModel").child(modelFan.getId()).child("name");
                        RefFanWatt = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("fanModel").child(modelFan.getId()).child("watt");
                        String edtnamefan = holder.edt_name_fan.getText().toString();
                        String edtwattfan = holder.edt_watt_fan.getText().toString();//20200930เพิ่ม เป็นการแปลง String เป็น integer
                        int finalewf= Integer.parseInt(edtwattfan);//20200930เพิ่ม
                        if (TextUtils.isEmpty(edtnamefan )& TextUtils.isEmpty(edtwattfan)) {
                            holder.btn_save_Fan.setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(), "กรุณาตั้งชื่อให้หลอดไฟ!!!", Toast.LENGTH_LONG).show();
                        } else {
                            holder.btn_save_Fan.setVisibility(View.GONE);
                            holder.btn_edit_Fan.setVisibility(View.VISIBLE);
                            holder.edt_name_fan.setFocusable(false);
                            holder.edt_watt_fan.setFocusable(false);
                            RefFan.setValue(holder.edt_name_fan.getText().toString());
                            RefFanWatt.setValue(finalewf);
                            Toast.makeText(getActivity(), "บันทึกแล้ว:)", Toast.LENGTH_LONG).show();
                            //เมื่อกดปุ่ม บันทึก ก็จะไม่สามารถ แก้ไขแบบดือๆ ได้แล้ว
                            holder.edt_name_fan.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    v.setFocusable(false);
                                    v.setFocusableInTouchMode(false);
                                    return false;
                                }
                            });
                            //20200930เพิ่ม
                            holder.edt_watt_fan.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    v.setFocusable(false);
                                    v.setFocusableInTouchMode(false);
                                    return false;
                                }
                            });
                        }
                    }
                });
                ////Touch Fan alert  กรุณากดปุ่มแก้ไข
                holder.edt_name_fan.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            Toast.makeText(getContext(), "กรุณากดปุ่มแก้ไข!", Toast.LENGTH_LONG).show();
                        }
                        return false;
                    }
                });
                //20200930เพิ่ม
                holder.edt_watt_fan.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction()==MotionEvent.ACTION_UP){
                            Toast.makeText(getContext(),"กรุณากดปุ่มแก้ไข!",Toast.LENGTH_LONG).show();
                        }
                        return false;
                    }
                });
            }

            @NonNull
            @Override
            public FanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.set_item_fan, parent, false);
                return new FanViewHolder(view);
            }
        };
        // Show amount Air in descLight นับจำนวนของ item จะแสดงเมื่อมีการเชื่อต่อเน็ตแล้วดึงข้อมูลมาให้เห็น
        RefFan.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final int countFan = (int) dataSnapshot.getChildrenCount();
                settingBinding.descFan.setText("พัดลมดูดอากาศทั้งหมด:" + countFan);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        // End Show amount Air in descLight จะแสดงเมื่อมีการเชื่อต่อเน็ตแล้วดึงข้อมูลมาให้เห็น
        adapterFan.startListening();
        recyclerViewFan.setAdapter(adapterFan);

        settingBinding.cvFan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settingBinding.recycleFan.getVisibility() == View.GONE) {
                    settingBinding.recycleFan.setVisibility(View.VISIBLE);
                    settingBinding.arrowBtnFan.setVisibility(View.VISIBLE);
                    settingBinding.nextarrowBtnFan.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(settingBinding.cvFan, new AutoTransition());
                } else {
                    settingBinding.recycleFan.setVisibility(View.GONE);
                    settingBinding.nextarrowBtnFan.setVisibility(View.VISIBLE);
                    settingBinding.arrowBtnFan.setVisibility(View.GONE);
                }
            }
        });
        settingBinding.nextarrowBtnFan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settingBinding.recycleFan.getVisibility() == View.GONE) {
                    settingBinding.recycleFan.setVisibility(View.VISIBLE);
                    settingBinding.arrowBtnFan.setVisibility(View.VISIBLE);
                    settingBinding.nextarrowBtnFan.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(settingBinding.cvFan, new AutoTransition());
                } else {
                    settingBinding.recycleFan.setVisibility(View.GONE);
                }
            }
        });
        settingBinding.arrowBtnFan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settingBinding.recycleFan.getVisibility() == View.VISIBLE) {
                    settingBinding.recycleFan.setVisibility(View.GONE);
                    settingBinding.arrowBtnFan.setVisibility(View.GONE);
                    settingBinding.nextarrowBtnFan.setVisibility(View.VISIBLE);
                }
            }
        });

        //Logout
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ProgressDialog pd = new ProgressDialog(getActivity());
//                pd.setTitle("เข้าสู่ระบบ");
//                pd.setMessage("กำลังเข้าสู่ระบบ");
//                pd.show();
                Prefs.clear();
                getActivity().finish();
//                mAuth.signOut();
//                android.os.Process.killProcess(android.os.Process.myPid());
//                System.exit(0);
            }
        });

        return v;
    }

    //LightViewHolder
    public class LightViewHolder extends RecyclerView.ViewHolder {
        EditText edt_name,edt_watt;
        TextView txtstatus;
        Button btn_edit_light, btn_save_light;

        public LightViewHolder(@NonNull View v) {
            super(v);
            edt_name = v.findViewById(R.id.edt_name);
            edt_watt = v.findViewById(R.id.edt_watt);
            txtstatus = v.findViewById(R.id.txt_status);
            btn_edit_light = v.findViewById(R.id.btn_edit_light);
            btn_save_light = v.findViewById(R.id.btn_save_light);
        }
    }

    //AirViewHolder
    public class AirViewHolder extends RecyclerView.ViewHolder {

        EditText edt_name_air,edt_watt_air;
        TextView txtstatusAir;
        Button btn_edit_air, btn_save_air;

        public AirViewHolder(@NonNull View v) {
            super(v);
            edt_name_air = v.findViewById(R.id.edt_name_air);
            edt_watt_air = v.findViewById(R.id.edt_watt_air);
            txtstatusAir = v.findViewById(R.id.txt_status_air);
            btn_edit_air = v.findViewById(R.id.btn_edit_air);
            btn_save_air = v.findViewById(R.id.btn_save_air);
        }
    }

    // FanViewHolder
    public class FanViewHolder extends RecyclerView.ViewHolder {

        EditText edt_name_fan,edt_watt_fan;
        TextView  txtstatusFan;
        Button btn_edit_Fan, btn_save_Fan;

        public FanViewHolder(@NonNull View v) {
            super(v);
            edt_name_fan = v.findViewById(R.id.edt_name_fan);
            edt_watt_fan = v.findViewById(R.id.edt_watt_fan);//20200930เพิ่ม
            txtstatusFan = v.findViewById(R.id.txt_status_fan);
            btn_edit_Fan = v.findViewById(R.id.btn_edit_fan);
            btn_save_Fan = v.findViewById(R.id.btn_save_fan);
        }
    }




}
