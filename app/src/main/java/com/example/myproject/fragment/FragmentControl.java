package com.example.myproject.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;

import com.example.myproject.Model.AirModel;
import com.example.myproject.Model.FanModel;
import com.example.myproject.Model.LightModel;
import com.example.myproject.R;
import com.example.myproject.databinding.FragmentControlBinding;
import com.example.myproject.services.AllDevice;
import com.example.myproject.services.ApiClient;
import com.example.myproject.services.ApiInterface;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.pixplicity.easyprefs.library.Prefs;
import com.suke.widget.SwitchButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentControl extends Fragment {

    String TAG="FragmentControl";
    FragmentControlBinding binding;

    DatabaseReference Ref, RefAir, RefFan;

    RecyclerView recyclerView ,recyclerViewAir,recyclerViewFan;

    FirebaseRecyclerOptions<LightModel> options;
    FirebaseRecyclerOptions<AirModel> optionsAir;
    FirebaseRecyclerOptions<FanModel> optionsFan;

    FirebaseRecyclerAdapter<LightModel, LightViewHolder> adapter;
    FirebaseRecyclerAdapter<AirModel, AirViewHolder> adapterAir;
    FirebaseRecyclerAdapter<FanModel, FanViewHolder> adapterFan;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_control, container, false);
        View v = binding.getRoot();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

        Ref = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("lightModel");

        options = new FirebaseRecyclerOptions.Builder<LightModel>()
                .setQuery(Ref, LightModel.class).build();

        recyclerView = v.findViewById(R.id.recycle_light);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new FirebaseRecyclerAdapter<LightModel, LightViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final LightViewHolder holder, int position, @NonNull final LightModel model) {
                holder.name.setText(model.getName());
                holder.status.setChecked(model.isStatus());
                if (model.isStatus()) {
                    holder.txtstatus.setText("เปิด");

                } else {
                    holder.txtstatus.setText("ปิด");
                }

                holder.status.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                        Ref = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("lightModel").child(model.getId()).child("status");
                        if (holder.status.isChecked()) {
                            create(model.getId(),model.getName(),model.getTypename(),model.getWatt());
                            Ref.setValue(true);//20200707 เปลียนจาก true  เป็น 1
                        } else {
                            update(model.getId(),model.getTypename());
//                            update(model.getId());
                            Ref.setValue(false);
                        }
                    }
                });
            }

            @NonNull
            @Override
            public LightViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_light, parent, false);
                return new LightViewHolder(view);
            }
        };

        adapter.startListening();
        recyclerView.setAdapter(adapter);


        binding.cvLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.recycleLight.getVisibility()== View.GONE){
                    binding.recycleLight.setVisibility(View.VISIBLE);
                    binding.arrowBtnLight.setVisibility(View.VISIBLE);
                    binding.nextarrowBtnLight.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(binding.cvLight,new AutoTransition());
                }
                else {
                    binding.recycleLight.setVisibility(View.GONE);
                    binding.nextarrowBtnLight.setVisibility(View.VISIBLE);
                    binding.arrowBtnLight.setVisibility(View.GONE);
                }
            }
        });

        binding.cvAir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.recycleAir.getVisibility()== View.GONE){
                    binding.recycleAir.setVisibility(View.VISIBLE);
                    binding.arrowBtnAir.setVisibility(View.VISIBLE);
                    binding.nextarrowBtnAir.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(binding.cvAir,new AutoTransition());
                }
                else {
                    binding.recycleAir.setVisibility(View.GONE);
                    binding.nextarrowBtnAir.setVisibility(View.VISIBLE);
                    binding.arrowBtnAir.setVisibility(View.GONE);
                }
            }
        });
        binding.cvFan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.recycleFan.getVisibility()== View.GONE){
                    binding.recycleFan.setVisibility(View.VISIBLE);
                    binding.arrowBtnFan.setVisibility(View.VISIBLE);
                    binding.nextarrowBtnFan.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(binding.cvFan,new AutoTransition());
                }
                else {
                    binding.recycleFan.setVisibility(View.GONE);
                    binding.nextarrowBtnFan.setVisibility(View.VISIBLE);
                    binding.arrowBtnFan.setVisibility(View.GONE);
                }
            }
        });
        binding.nextarrowBtnLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.recycleLight.getVisibility()== View.GONE){
                    binding.recycleLight.setVisibility(View.VISIBLE);
                    binding.arrowBtnLight.setVisibility(View.VISIBLE);
                    binding.nextarrowBtnLight.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(binding.cvLight,new AutoTransition());
                }
                else {
                    binding.recycleLight.setVisibility(View.GONE);
                }
            }
        });

        binding.arrowBtnLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.recycleLight.getVisibility() == View.VISIBLE) {
                    binding.recycleLight.setVisibility(View.GONE);
                    binding.arrowBtnLight.setVisibility(View.GONE);
                    binding.nextarrowBtnLight.setVisibility(View.VISIBLE);
                }
            }
        });

        getCount();
        Log.d("TAG", "ID: "+Prefs.getString("Id", "null"));
        // start Air
        LinearLayoutManager layoutManagerAir = new LinearLayoutManager(getContext());
        RefAir = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("airModel");

        optionsAir = new FirebaseRecyclerOptions.Builder<AirModel>()
                .setQuery(RefAir, AirModel.class).build();

        recyclerViewAir = v.findViewById(R.id.recycle_air);
        recyclerViewAir.setLayoutManager(layoutManagerAir);

        adapterAir = new FirebaseRecyclerAdapter<AirModel, AirViewHolder>(optionsAir) {
            @Override
            protected void onBindViewHolder(@NonNull final AirViewHolder holderAir, int position, @NonNull final AirModel modelAir) {
                holderAir.nameAir.setText(modelAir.getName());
                holderAir.statusAir.setChecked(modelAir.isStatus());
                if (modelAir.isStatus()) {
                    holderAir.txtstatusAir.setText("เปิด");
                } else {
                    holderAir.txtstatusAir.setText("ปิด");
                }

                holderAir.statusAir.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                        RefAir = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("airModel").child(modelAir.getId()).child("status");
                        if (holderAir.statusAir.isChecked()) {
                            create(modelAir.getId(),modelAir.getName(),modelAir.getTypename(),modelAir.getWatt());//20200930 copy code ที่พี่เอกพงค์ทำ มาวางดูอ่ะ ปรากฏว่า ได้ ใช้งานได้
                            RefAir.setValue(true);
                        } else {
                            update(modelAir.getId(),modelAir.getTypename());
//                            update(modelAir.getId());
                            RefAir.setValue(false);
                        }
                    }
                });
            }

            @NonNull
            @Override
            public AirViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_air, parent, false);
                return new AirViewHolder(view);
            }
        };

        adapterAir.startListening();
        recyclerViewAir.setAdapter(adapterAir);

        binding.arrowBtnAir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.recycleAir.getVisibility() == View.VISIBLE) {
                    binding.recycleAir.setVisibility(View.GONE);
                    binding.nextarrowBtnAir.setVisibility(View.VISIBLE);
                    binding.arrowBtnAir.setVisibility(View.GONE);
                } else {
                    binding.recycleAir.setVisibility(View.VISIBLE);
                }
            }
        });
        binding.nextarrowBtnAir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.recycleAir.getVisibility() == View.GONE){
                    binding.recycleAir.setVisibility(View.VISIBLE);
                    binding.nextarrowBtnAir.setVisibility(View.GONE);
                    binding.arrowBtnAir.setVisibility(View.VISIBLE);
                    TransitionManager.beginDelayedTransition(binding.cvAir,new AutoTransition());

                }
            }
        });
        getCountAir();

        // start Fan
        LinearLayoutManager layoutManagerFan = new LinearLayoutManager(getContext());
        RefFan = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("fanModel");

        optionsFan = new FirebaseRecyclerOptions.Builder<FanModel>()
                .setQuery(RefFan, FanModel.class).build();

        recyclerViewFan = v.findViewById(R.id.recycle_fan);
        recyclerViewFan.setLayoutManager(layoutManagerFan);

        adapterFan = new FirebaseRecyclerAdapter<FanModel, FanViewHolder>(optionsFan) {
            @Override
            protected void onBindViewHolder(@NonNull final FanViewHolder holderFan, int position, @NonNull final FanModel modelFan) {
                holderFan.nameFan.setText(modelFan.getName());
                holderFan.statusFan.setChecked(modelFan.isStatus());
                if (modelFan.isStatus()) {
                    holderFan.txtstatusFan.setText("เปิด");
                } else {
                    holderFan.txtstatusFan.setText("ปิด");
                }

                holderFan.statusFan.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                        RefFan = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("fanModel").child(modelFan.getId()).child("status");
                        if (holderFan.statusFan.isChecked()) {
                            create(modelFan.getId(),modelFan.getName(),modelFan.getTypename(),modelFan.getWatt());
                            RefFan.setValue(true);
                        } else {
                            update(modelFan.getId(),modelFan.getTypename());
//                            update(modelFan.getId());
                            RefFan.setValue(false);
                        }
                    }
                });
            }

            @NonNull
            @Override
            public FanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_fan, parent, false);
                return new FanViewHolder(view);
            }
        };

        adapterFan.startListening();
        recyclerViewFan.setAdapter(adapterFan);

        binding.arrowBtnFan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.recycleFan.getVisibility() == View.VISIBLE) {
                    binding.recycleFan.setVisibility(View.GONE);
                    binding.arrowBtnFan.setVisibility(View.GONE);
                    binding.nextarrowBtnFan.setVisibility(View.VISIBLE);
                } else {
                    binding.recycleFan.setVisibility(View.VISIBLE);
                }
            }
        });
        binding.nextarrowBtnFan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.recycleFan.getVisibility() == View.GONE){
                    binding.recycleFan.setVisibility(View.VISIBLE);
                    binding.nextarrowBtnFan.setVisibility(View.GONE);
                    binding.arrowBtnFan.setVisibility(View.VISIBLE);
                    TransitionManager.beginDelayedTransition(binding.cvFan,new AutoTransition());


                }
            }
        });
        getCountFan();
        return v;
    }
    //getCountLight
    public void getCount() {
        Ref = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("lightModel");
        Ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final int count = (int) dataSnapshot.getChildrenCount();
                binding.descLight.setText("หลอดไฟทั้งหมด: " + count);

                binding.onOffAll.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                        if (binding.onOffAll.isChecked()) {
                            for (int i = 0; i < count; i++) {
                                Ref = FirebaseDatabase.getInstance().getReference().child(Prefs.getString("Id", "null")+"/lightModel").child("" + i).child("status");
                                Ref.setValue(true);
                            }
                        } else {
                            for (int i = 0; i < count; i++) {
                                Ref = FirebaseDatabase.getInstance().getReference().child(Prefs.getString("Id", "null")+"/lightModel").child("" + i).child("status");
                                Ref.setValue(false);
                            }
                        }
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    //GetCountAir
    public void getCountAir() {
        RefAir = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("airModel");
        RefAir.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final int countAir = (int) dataSnapshot.getChildrenCount();
                binding.descAir.setText("แอร์ทั้งหมด:" + countAir);

                binding.onOffAllAir.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                        if (binding.onOffAllAir.isChecked()) {
                            for (int i = 0; i < countAir; i++) {
                                RefAir = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("airModel").child("" + i).child("status");
                                RefAir.setValue(true);
                            }
                        } else {
                            for (int i = 0; i < countAir; i++) {
                                RefAir = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("airModel").child("" + i).child("status");
                                RefAir.setValue(false);
                            }
                        }

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    //endGetCountAir

    //GetCountFan
    public void getCountFan() {
        RefFan = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("fanModel");
        RefFan.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final int countFan = (int) dataSnapshot.getChildrenCount();
                binding.descFan.setText("พัดลมระบายอากาศทั้งหมด:" + countFan);

                binding.onOffAllFan.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                        if (binding.onOffAllFan.isChecked()) {
                            for (int i = 0; i < countFan; i++) {
                                RefFan = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("fanModel").child("" + i).child("status");
                                RefFan.setValue(true);
                            }
                        } else {
                            for (int i = 0; i < countFan; i++) {
                                RefFan = FirebaseDatabase.getInstance().getReference(Prefs.getString("Id", "null")).child("fanModel").child("" + i).child("status");
                                RefFan.setValue(false);
                            }
                        }
                    }
                });
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
    //endGetCountFan

    //LightViewHolder
    public static class LightViewHolder extends RecyclerView.ViewHolder {

        TextView name, txtstatus;
        SwitchButton status;

        public LightViewHolder(@NonNull View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.txt_name);
            txtstatus = (TextView) v.findViewById(R.id.txt_status);
            status = (SwitchButton) v.findViewById(R.id.btn_switch);
        }
    }
    //AirViewHolder
    public class AirViewHolder extends RecyclerView.ViewHolder {

        TextView nameAir, txtstatusAir;
        SwitchButton statusAir;

        public AirViewHolder(@NonNull View v) {
            super(v);
            nameAir =  v.findViewById(R.id.txt_name_air);
            txtstatusAir =  v.findViewById(R.id.txt_status_air);
            statusAir = v.findViewById(R.id.btn_switch_air);
        }
    }
    // FanViewHolder
    public class FanViewHolder extends RecyclerView.ViewHolder {

        TextView nameFan, txtstatusFan;
        SwitchButton statusFan;

        public FanViewHolder(@NonNull View v) {
            super(v);
            nameFan =  v.findViewById(R.id.txt_name_fan);
            txtstatusFan =  v.findViewById(R.id.txt_status_fan);
            statusFan = v.findViewById(R.id.btn_switch_fan);
        }
    }

//Retrofit
    public void create(String device_id,String device_name,String typename ,int device_watt){
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        final Call<List<AllDevice>> create = apiInterface.create(Prefs.getString("Id", "null"),device_id,device_name,typename,device_watt);
        create.enqueue(new Callback<List<AllDevice>>() {
            @Override
            public void onResponse(Call<List<AllDevice>> call, Response<List<AllDevice>> response) {
                if (response.isSuccessful()){
                    Log.d(TAG, "onResponse: "+response.body());
                }
            }

            @Override
            public void onFailure(Call<List<AllDevice>> call, Throwable t) {

            }
        });
    }

    public void update(String device_id,String typename  ){
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        final Call<List<AllDevice>> update;
        update = apiInterface.update(device_id,typename);
        update.enqueue(new Callback<List<AllDevice>>() {
            @Override
            public void onResponse(Call<List<AllDevice>> call, Response<List<AllDevice>> response) {
                if (response.isSuccessful()){
                    Log.d(TAG, "onResponse: "+response.body());
                }
            }

            @Override
            public void onFailure(Call<List<AllDevice>> call, Throwable t) {
                Log.d(TAG, "onResponse: "+t.getMessage());
            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();

    }
}
