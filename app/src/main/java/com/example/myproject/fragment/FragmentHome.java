package com.example.myproject.fragment;

import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.Selection;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myproject.MainActivity;
import com.example.myproject.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

import java.nio.channels.SelectionKey;


public class FragmentHome extends Fragment {

    CardView cardcontrol,cardreport,cardsetting;

    BottomNavigationView navItem;
    String Uid;

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_home, container, false);

        cardcontrol = v.findViewById(R.id.cv_id);
        cardreport = v.findViewById(R.id.cv_report);
        cardsetting = v.findViewById(R.id.cv_setting);
        Uid = FirebaseAuth.getInstance().getCurrentUser().getUid();

        Log.d("TAG", "onCreateView: "+ FirebaseAuth.getInstance().getCurrentUser().getUid());
        navItem = getActivity().findViewById(R.id.bottom_navigation);

        cardcontrol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment(new FragmentControl());
                navItem.setSelectedItemId(R.id.control);
        }

        });

        cardreport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment(new FragmentReport());
                navItem.setSelectedItemId(R.id.report);

            }
        });

        cardsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment(new FragmentSetting());
                navItem.setSelectedItemId(R.id.settings);

            }
        });

        return v;
    }

    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
    }




}
