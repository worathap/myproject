package com.example.myproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ResetPasswordActivity extends AppCompatActivity {

    EditText edt_email;
    Button btn_reset;
    FirebaseAuth firebaseAuth;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        edt_email=findViewById(R.id.edtemail);
        btn_reset= findViewById(R.id.btn_reset);
        firebaseAuth = FirebaseAuth.getInstance();

        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sendemail = edt_email.getText().toString();
                if (TextUtils.isEmpty(sendemail)){
                    edt_email.setError("กรุณาตรวจสอบอีเมล์");
                    edt_email.requestFocus();
                }else {
                    firebaseAuth.sendPasswordResetEmail(sendemail).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (!task.isSuccessful()){
                                String message = task.getException().getMessage();
                                Toast.makeText(ResetPasswordActivity.this,"error:"+message,Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(ResetPasswordActivity.this, "ตรวจสอบกล่องอีเมลเพื่อตั้งรหัสผ่านใหม่", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(ResetPasswordActivity.this,SignInActivity.class));
                            }
                        }
                    });
                }
            }
        });
    }
}
