package com.example.myproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ReportFragment;

import android.app.DownloadManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.myproject.fragment.FragmentControl;
import com.example.myproject.fragment.FragmentHome;
import com.example.myproject.fragment.FragmentReport;
import com.example.myproject.fragment.FragmentSetting;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView navItem;

    private ActionBar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = getSupportActionBar();
        navItem =(BottomNavigationView)findViewById(R.id.bottom_navigation);

        setFragment(new FragmentHome());

        navItem.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home:
                        setFragment(new FragmentHome());
                        toolbar.setTitle("หน้าหลัก");
                        break;
                    case R.id.control:
                        setFragment(new FragmentControl());
                        toolbar.setTitle("ควบคุมอการเปิด-ปิดุปกรณ์");
                        break;
                    case R.id.report:
                        setFragment(new FragmentReport());
                        toolbar.setTitle("แสดงรายงาน");
                        break;
                    case R.id.settings:
                        setFragment(new FragmentSetting());
                        toolbar.setTitle("ตั้งค่า");
                        break;
                }
                return true;
            }
        });
    }


    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    public void backache(View view) {
        Intent back=new Intent(MainActivity.this,FragmentHome.class);
        finish();
    }


    long back_pressed;
    @Override
    public void onBackPressed() {
        if (back_pressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
            finish();
        } else
        Toast.makeText(getApplicationContext(), "กรุณากดอีกครั้งเพื่อออกจากระบบ!", Toast.LENGTH_SHORT).show();
        back_pressed = System.currentTimeMillis();

    }
}
