package com.example.myproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.pixplicity.easyprefs.library.Prefs;

public class loadingpage extends AppCompatActivity {

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loadingpage);

        progressBar = findViewById(R.id.progressBar);
        progressBar.setMax(100);
        progressBar.setProgress(0);

        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    for (int i = 0; i<100; i++ ){
                        progressBar.setProgress(i);
                        sleep(20);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    startActivity(new Intent(getApplicationContext(),SignInActivity.class));
                    finish();
                }
            }
        };
        thread.start();
    }

    public void checkLogin(){
    /*    String User =  Prefs.getString("Id", "null");
        String Password = Prefs.getString("Pass","null");*/
        if (Prefs.getString("Id", "null") == null){
            startActivity(new Intent(getApplicationContext(),SignInActivity.class));
            finish();
        }else {
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
            finish();
        }
    }
}
